(function () {
    'use strict';

    angular.module('likitana.categorie')
        .controller('CategorieReadsCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'CategorieService', CategorieReadsCtrl])
        .controller('CategorieCreateCtrl', ['$mdToast', '$location', 'CategorieService', CategorieCreateCtrl])
        .controller('CategorieUpdateCtrl', ['$location', '$mdToast', '$stateParams', 'CategorieService', CategorieUpdateCtrl]);

    function CategorieReadsCtrl($mdToast, $mdDialog, $state, $stateParams, $location, CategorieService) {
        /* jshint validthis: true */
        var vm = this;
        vm.confirmSuppression = confirmSuppression;
        vm.goUpdate = goUpdate;

        getCategories();

        /*    ---  Call API ---    */
        function getCategories(){
            CategorieService.getCategories()
                .then(getCategoriesSuccess, getCategoriesError);
        }

        function deleteCategorie(id) {
            CategorieService.deleteCategorie(id)
                .then(deleteCategorieSuccess, deleteCategorieError);
        }

        /*    ---  Handle Response ---    */
        function getCategoriesSuccess(response){
            vm.categories = response.data;
        }

        function getCategoriesError(error){
            Error(error)
        }

        function deleteCategorieSuccess(response){
            Reload();
        }

        function deleteCategorieError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function confirmSuppression(ev, categorie){
            var confirm = $mdDialog.confirm()
                .title('Suppression')
                .content('Voulez vous supprimer cette catégorie ?')
                .ariaLabel('Suppression')
                .targetEvent(ev)
                .ok('Supprimer')
                .cancel('Annuler');
            $mdDialog.show(confirm).then(function () {
                deleteCategorie(categorie.id);
            });
        }

        function Reload(){
            $state.transitionTo($state.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        }

        /*    ---  Redirect ---    */
        function goUpdate(id){
            $location.url('/categories/update/'+id);
        }

    }

    function CategorieCreateCtrl($mdToast, $location, CategorieService) {
        /* jshint validthis: true */
        var vm = this;
        vm.create = createCategorie;

        /*    ---  Call API ---    */
        function createCategorie (categorie) {
            CategorieService.createCategorie(categorie)
                .then(createCategorieSuccess, createCategorieError);
        }

        /*    ---  Handle Response ---    */
        function createCategorieSuccess (response) {
            $location.url('/categories');
        }

        function createCategorieError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }
    }

    function CategorieUpdateCtrl($location, $mdToast, $stateParams, CategorieService) {
        /* jshint validthis: true */
        var vm = this;
        vm.update = updateCategorie;

        var id = $stateParams.id;

        getCategorie();

        /*    ---  Call API ---    */
        function getCategorie(){
            CategorieService.getCategorie(id)
                .then(getCategorieSuccess, getCategorieError);
        }

        function updateCategorie(categorie) {
            CategorieService.updateCategorie(categorie, id)
                .then(updateCategorieSuccess, updateCategorieError);
        }

        /*    ---  Handle Response ---    */
        function getCategorieSuccess(response){
            vm.categorie = response.data;
        }

        function getCategorieError(error){
            Error(error);
        }

        function updateCategorieSuccess (response){
            $location.url('/categories');
        }

        function updateCategorieError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(3000)
            );
        }

    }
})();
