(function () {
    'use strict';

    angular.module('likitana.categorie')
        .service("CategorieService", CategorieService);

    function CategorieService($http, $cookies){
        this.getCategories = getCategories;
        this.getCategorie = getCategorie;
        this.createCategorie = createCategorie;
        this.updateCategorie = updateCategorie;
        this.deleteCategorie = deleteCategorie;

        function getCategories(){
            return  $http.get('/categories',
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function getCategorie(id){
            return  $http.get('/categories/'+id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function createCategorie(categorie){
            return $http.post('/categories',
                {
                    "categorie": categorie.categorie
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function updateCategorie(categorie, id){
            return  $http.put('/categories',
                {
                    "id": id,
                    "categorie": categorie.categorie
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function deleteCategorie(id){
            return  $http.delete('/categories/' + id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }
    }

})();
