(function () {
    'use strict';

    angular.module('likitana.destination')
        .controller('DestinationReadsCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'DestinationService', DestinationReadsCtrl])
        .controller('DestinationCreateCtrl', ['$mdToast', '$location', 'DestinationService', DestinationCreateCtrl])
        .controller('DestinationUpdateCtrl', ['$location', '$mdToast', '$stateParams', 'DestinationService', DestinationUpdateCtrl]);

    function DestinationReadsCtrl($mdToast, $mdDialog, $state, $stateParams, $location, DestinationService) {
        /* jshint validthis: true */
        var vm = this;
        vm.confirmSuppression = confirmSuppression;
        vm.goUpdate = goUpdate;

        getDestinations();

        /*    ---  Call API ---    */
        function getDestinations(){
            DestinationService.getDestinations()
                .then(getDestinationsSuccess, getDestinationsError);
        }

        function deleteDestination(id) {
            DestinationService.deleteDestination(id)
                .then(deleteDestinationSuccess, deleteDestinationError);
        }

        /*    ---  Handle Response ---    */
        function getDestinationsSuccess(response){
            vm.destinations = response.data;
        }

        function getDestinationsError(error){
            Error(error)
        }

        function deleteDestinationSuccess(response){
            Reload();
        }

        function deleteDestinationError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function confirmSuppression(ev, destination){
            var confirm = $mdDialog.confirm()
                .title('Suppression')
                .content('Voulez vous supprimer cette catégorie ?')
                .ariaLabel('Suppression')
                .targetEvent(ev)
                .ok('Supprimer')
                .cancel('Annuler');
            $mdDialog.show(confirm).then(function () {
                deleteDestination(destination.id);
            });
        }

        function Reload(){
            $state.transitionTo($state.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        }

        /*    ---  Redirect ---    */
        function goUpdate(id){
            $location.url('/destinations/update/'+id);
        }

    }

    function DestinationCreateCtrl($mdToast, $location, DestinationService) {
        /* jshint validthis: true */
        var vm = this;
        vm.create = createDestination;

        /*    ---  Call API ---    */
        function createDestination (destination) {
            DestinationService.createDestination(destination)
                .then(createDestinationSuccess, createDestinationError);
        }

        /*    ---  Autres ---    */
        function createDestinationSuccess (response) {
            $location.url('/destinations');
        }

        function createDestinationError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }
    }

    function DestinationUpdateCtrl($location, $mdToast, $stateParams, DestinationService) {
        /* jshint validthis: true */
        var vm = this;
        vm.update = updateDestination;

        var id = $stateParams.id;

        getDestination();

        /*    ---  Call API ---    */
        function getDestination(){
            DestinationService.getDestination(id)
                .then(getDestinationSuccess, getDestinationError);
        }

        function updateDestination(destination) {
            DestinationService.updateDestination(destination, id)
                .then(updateDestinationSuccess, updateDestinationError);
        }

        /*    ---  Handle Response ---    */
        function getDestinationSuccess(response){
            vm.destination = response.data;
        }

        function getDestinationError(error){
            Error(error);
        }

        function updateDestinationSuccess (response){
            $location.url('/destinations');
        }

        function updateDestinationError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(3000)
            );
        }

    }
})();
