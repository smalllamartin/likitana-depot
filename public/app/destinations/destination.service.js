(function () {
    'use strict';

    angular.module('likitana.destination')
        .service("DestinationService", DestinationService);

    function DestinationService($http, $cookies){
        this.getDestinations = getDestinations;
        this.getDestination = getDestination;
        this.createDestination = createDestination;
        this.updateDestination = updateDestination;
        this.deleteDestination = deleteDestination;

        function getDestinations(){
            return  $http.get('/destinations',
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function getDestination(id){
            return  $http.get('/destinations/'+id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function createDestination(destination){
            return $http.post('/destinations',
                {
                    "destination": destination.destination
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function updateDestination(destination, id){
            return  $http.put('/destinations',
                {
                    "id": id,
                    "destination": destination.destination
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function deleteDestination(id){
            return  $http.delete('/destinations/' + id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }
    }

})();
