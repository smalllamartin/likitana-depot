(function () {
    'use strict';

    angular.module('likitana.invoice')
        .controller('InvoiceReadsCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'InvoiceService', InvoiceReadsCtrl])
        .controller('InvoiceReadCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'InvoiceService', InvoiceReadCtrl]);

    function InvoiceReadsCtrl($mdToast, $mdDialog, $state, $stateParams, $location, InvoiceService) {
        /* jshint validthis: true */
        var vm = this;
        vm.goInvoice = goInvoice;
        getInvoices();

        /*    ---  Call API ---    */
        function getInvoices(){
            InvoiceService.getInvoices()
                .then(getInvoicesSuccess, getInvoicesError);
        }

        /*    ---  Handle Response ---    */
        function getInvoicesSuccess(response){
            vm.invoices = response.data;
        }

        function getInvoicesError(error){
            Error(error)
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        /*    ---  Redirect ---    */
        function goInvoice(reference){
            $location.url('/invoices/'+reference);
        }

    }

    function InvoiceReadCtrl($mdToast, $mdDialog, $state, $stateParams, $location, InvoiceService) {
        /* jshint validthis: true */
        var vm = this;
        var reference = $stateParams.reference;

        getInvoice();

        /*    ---  Call API ---    */
        function getInvoice(){
            InvoiceService.getInvoice(reference)
                .then(getInvoicesSuccess, getInvoicesError);
        }

        /*    ---  Handle Response ---    */
        function getInvoicesSuccess(response){
            vm.invoices = response.data;
        }

        function getInvoicesError(error){
            Error(error)
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        /*    ---  Redirect ---    */
    }

})();
