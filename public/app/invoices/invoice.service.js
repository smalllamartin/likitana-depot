(function () {
    'use strict';

    angular.module('likitana.invoice')
        .service("InvoiceService", InvoiceService);

    function InvoiceService($http, $cookies){
        this.getInvoices = getInvoices;
        this.getInvoice = getInvoice;
        this.createInvoice = createInvoice;

        function getInvoices(){
            return  $http.get('/invoices/references',
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function getInvoice(reference){
            return  $http.get('/invoices/references/'+reference,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function createInvoice(invoice, reference){
            return $http.post('/invoices',
                {
                    "reference" : reference,
                    "type": invoice.type,
                    "nature" : invoice.nature,
                    "montant" : invoice.montant
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

    }

})();
