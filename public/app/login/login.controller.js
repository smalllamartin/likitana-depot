(function () {
    'use strict';

    angular.module('likitana.login')
        .controller('loginCtrl', ['$scope', '$http', '$mdDialog', '$mdToast', '$state', '$stateParams', '$cookies', '$location', loginCtrl])
        .controller('profilCtrl', ['$scope', '$http', '$location', '$mdToast', '$stateParams', '$cookies', profilCtrl]);

    function loginCtrl($scope, $http, $mdDialog, $mdToast, $state, $stateParams, $cookies, $location) {
        $scope.login = function (user) {

            $http.post('/login',
                {
                    "telephone": user.telephone, "password": user.password
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            )
                .success(function (data, status, headers, config, statusText) {

                    if (status == '200' || status == '301') {
                        $location.url('/accueil');
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .content('Téléphone ou mot de passe incorrect')
                                .position('top right')
                                .hideDelay(3000)
                        );
                    }
                })
                .error(function (data, status, headers, config, statusText) {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Echec de connexion. Veuillez vérifier la connexion au système')
                            .position('top right')
                            .hideDelay(3000)
                    );
                });

        };
    }

    function profilCtrl($scope, $http, $location, $mdToast, $stateParams, $cookies) {
        $http.get('/utilisateurs/' + $stateParams.id,
            {
                headers: {'X-Auth-Token': $cookies.get('authToken')}
            })
            .success(function (data, status, headers, config, statusText) {
                if (status == '200' || status == '301') {
                    $scope.utilisateur = data;
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Echec de chargement de la liste. Veuillez vérifier la connexion à la base de donnée!')
                            .position('top right')
                            .hideDelay(3000)
                    );
                }
            })
            .error(function (data, status, headers, config, statusText) {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Echec de chargement de la liste. Veuillez vérifier la connexion au système')
                        .position('top right')
                        .hideDelay(3000)
                );
            });
        $scope.modification = function (utilisateur) {
            $http.put('/utilisateurs',
                {
                    "id": $stateParams.id,
                    "nom": utilisateur.nom,
                    "prenom": utilisateur.prenom,
                    "telephone": utilisateur.telephone,
                    "password": utilisateur.password,
                    "profil": utilisateur.profil
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            )
                .success(function (data, status, headers, config, statusText) {

                    if (status == '200' || status == '301') {
                        $location.url('/utilisateurs');
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .content('La utilisateur n\'a pas été modifiée. Veuillez vérifier la connexion à la base de donnée!')
                                .position('top right')
                                .hideDelay(3000)
                        );
                    }
                })
                .error(function (data, status, headers, config, statusText) {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('La utilisateur n\'a pas été modifiée. Veuillez vérifier la connexion au système!')
                            .position('top right')
                            .hideDelay(3000)
                    );
                });
        }
    }
})(); 