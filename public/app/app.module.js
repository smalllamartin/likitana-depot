(function () {
    'use strict';

    angular.module('likitana', [
        // Core modules
        'likitana.core'

        // likitana
        , 'likitana.categorie'
        , 'likitana.gift'
        , 'likitana.destination'
        , 'likitana.input'
        , 'likitana.invoice'
        , 'likitana.product'
        , 'likitana.program'
        , 'likitana.origin'
        , 'likitana.output'
        , 'likitana.rapport'
        , 'likitana.sale'
        , 'likitana.user'
        , 'likitana.login'

        // Custom Feature modules
        ,'app.chart'


        // 3rd party feature modules
        ,'ui.tree'
        ,'ngMap'
        ,'textAngular'

    ]);

})();

