(function () {
    'use strict';

    angular.module('likitana.output')
        .controller('OutputReadsCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'OutputService', OutputReadsCtrl]);

    function OutputReadsCtrl($mdToast, $mdDialog, $state, $stateParams, $location, OutputService) {
        /* jshint validthis: true */
        var vm = this;
        vm.goProduct = goProduct;
        vm.doTraiter = doTraiter;

        getOutputs();

        /*    ---  Call API ---    */
        function getOutputs(){
            OutputService.getOutputs()
                .then(getOutputsSuccess, getOutputsError);
        }

        function updateOutput(id){
            OutputService.updateOutput(id)
                .then(updateOutputSuccess, updateOutputError);
        }

        /*    ---  Handle Response ---    */
        function getOutputsSuccess(response){
            vm.outputs = response.data;
        }

        function getOutputsError(error){
            Error(error)
        }

        function updateOutputSuccess(response){
            Reload();
        }

        function updateOutputError(error){
            Error(error)
        }


        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function doTraiter(id){
            updateOutput(id)
        }

        function Reload(){
            $state.transitionTo($state.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        }

        /*    ---  Redirect ---    */
        function goProduct(id){
            $location.url('/products/'+id);
        }
    }

})();
