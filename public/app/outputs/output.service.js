(function () {
    'use strict';

    angular.module('likitana.output')
        .service("OutputService", OutputService);

    function OutputService($http, $cookies){
        this.getOutputs = getOutputs;
        this.updateOutput = updateOutput;

        function getOutputs(){
            return  $http.get('/outputs',
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function updateOutput(id){
            console.log(id);
            return  $http.put('/outputs/'+id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

    }

})();
