(function () {
    'use strict';

    angular.module('likitana.program')
        .controller('ProgramReadsCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'ProgramService', ProgramReadsCtrl])
        .controller('ProgramCreateCtrl', ['$mdToast', '$location', 'ProgramService', ProgramCreateCtrl])
        .controller('ProgramUpdateCtrl', ['$location', '$mdToast', '$stateParams', 'ProgramService', ProgramUpdateCtrl]);

    function ProgramReadsCtrl($mdToast, $mdDialog, $state, $stateParams, $location, ProgramService) {
        /* jshint validthis: true */
        var vm = this;
        vm.confirmSuppression = confirmSuppression;
        vm.goUpdate = goUpdate;

        getPrograms();

        /*    ---  Call API ---    */
        function getPrograms(){
            ProgramService.getPrograms()
                .then(getProgramsSuccess, getProgramsError);
        }

        function deleteProgram(id) {
            ProgramService.deleteProgram(id)
                .then(deleteProgramSuccess, deleteProgramError);
        }

        /*    ---  Handle Response ---    */
        function getProgramsSuccess(response){
            vm.programs = response.data;
        }

        function getProgramsError(error){
            Error(error)
        }

        function deleteProgramSuccess(response){
            Reload();
        }

        function deleteProgramError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function confirmSuppression(ev, program){
            var confirm = $mdDialog.confirm()
                .title('Suppression')
                .content('Voulez vous supprimer cette catégorie ?')
                .ariaLabel('Suppression')
                .targetEvent(ev)
                .ok('Supprimer')
                .cancel('Annuler');
            $mdDialog.show(confirm).then(function () {
                deleteProgram(program.id);
            });
        }

        function Reload(){
            $state.transitionTo($state.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        }

        function goUpdate(id){
            $location.url('/programs/update/'+id);
        }

    }

    function ProgramCreateCtrl($mdToast, $location, ProgramService) {
        /* jshint validthis: true */
        var vm = this;
        vm.create = createProgram;

        /*    ---  Call API ---    */
        function createProgram (program) {
            ProgramService.createProgram(program)
                .then(createProgramSuccess, createProgramError);
        }

        /*    ---  Autres ---    */
        function createProgramSuccess (response) {
            $location.url('/programs');
        }

        function createProgramError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }
    }

    function ProgramUpdateCtrl($location, $mdToast, $stateParams, ProgramService) {
        /* jshint validthis: true */
        var vm = this;
        vm.update = updateProgram;

        var id = $stateParams.id;

        getProgram();

        /*    ---  Call API ---    */
        function getProgram(){
            ProgramService.getProgram(id)
                .then(getProgramSuccess, getProgramError);
        }

        function updateProgram(program) {
            ProgramService.updateProgram(program, id)
                .then(updateProgramSuccess, updateProgramError);
        }

        /*    ---  Handle Response ---    */
        function getProgramSuccess(response){
            vm.program = response.data;
        }

        function getProgramError(error){
            Error(error);
        }

        function updateProgramSuccess (response){
            $location.url('/programs');
        }

        function updateProgramError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(3000)
            );
        }

    }
})();
