(function () {
    'use strict';

    angular.module('likitana.program')
        .service("ProgramService", ProgramService);

    function ProgramService($http, $cookies){
        this.getPrograms = getPrograms;
        this.getProgram = getProgram;
        this.createProgram = createProgram;
        this.updateProgram = updateProgram;
        this.deleteProgram = deleteProgram;

        function getPrograms(){
            return  $http.get('/programs',
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function getProgram(id){
            return  $http.get('/programs/'+id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function createProgram(program){
            return $http.post('/programs',
                {
                    "program": program.program
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function updateProgram(program, id){
            return  $http.put('/programs',
                {
                    "id": id,
                    "program": program.program
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function deleteProgram(id){
            return  $http.delete('/programs/' + id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }
    }

})();
