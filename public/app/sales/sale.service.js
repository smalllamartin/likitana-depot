(function () {
    'use strict';

    angular.module('likitana.sale')
        .service("SaleService", SaleService);

    function SaleService($http, $cookies){
        this.getSales = getSales;
        this.getSale = getSale;
        this.createSale = createSale;
        this.updateSale = updateSale;

        function getSales(){
            return  $http.get('/outputs/references',
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function getSale(reference){
            return  $http.get('/outputs/references/'+reference,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function createSale(sale, reference){
            return $http.post('/sales',
                {
                    "reference" : reference,
                    "lot": sale.lot,
                    "quantity" : sale.quantitySale,
                    "price" : sale.price
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function updateSale(reference){
            return  $http.put('/outputs/references/'+reference,
                {
                    "destination" : 'client comptoir',
                    "type" : 'vente'
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

    }

})();
