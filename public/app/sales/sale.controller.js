(function () {
    'use strict';

    angular.module('likitana.sale')
        .controller('SaleReadsCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'SaleService', SaleReadsCtrl])
        .controller('SaleReadCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'SaleService', 'InvoiceService', SaleReadCtrl])
        .controller('SaleCreateCtrl', ['$mdToast', '$state', '$stateParams', '$location',  'SaleService', 'InputService', SaleCreateCtrl]);

    function SaleReadsCtrl($mdToast, $mdDialog, $state, $stateParams, $location, SaleService) {
        /* jshint validthis: true */
        var vm = this;
        vm.goTraiter = goTraiter;

        getSales();

        /*    ---  Call API ---    */
        function getSales(){
            SaleService.getSales()
                .then(getSalesSuccess, getSalesError);
        }

        /*    ---  Handle Response ---    */
        function getSalesSuccess(response){
            vm.sales = response.data;
        }

        function getSalesError(error){
            Error(error)
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        /*    ---  Redirect ---    */
        function goTraiter(reference){
            $location.url('/sales/'+reference);
        }

    }

    function SaleReadCtrl($mdToast, $mdDialog, $state, $stateParams, $location, SaleService, InvoiceService) {
        /* jshint validthis: true */
        var vm = this;
        vm.doTraiter = doTraiter;
        vm.doImprimer = doImprimer;
        vm.createInvoice = createInvoice;

        var reference = $stateParams.reference;

        getSale();

        /*    ---  Call API ---    */
        function getSale(){
            SaleService.getSale(reference)
                .then(getSalesSuccess, getSalesError);
        }

        function updateSale(){
            SaleService.updateSale(reference)
                .then(updateSaleSuccess, updateSaleError);
        }

        function createInvoice(invoice){
            InvoiceService.createInvoice(invoice, reference)
                .then(invoiceCreateSuccess, invoiceCreateError);
        }

        /*    ---  Handle Response ---    */
        function getSalesSuccess(response){
            vm.sales = response.data;
        }

        function getSalesError(error){
            Error(error)
        }

        function updateSaleSuccess(response){
            goGifts();
        }

        function updateSaleError(error){
            Error(error)
        }

        function invoiceCreateSuccess(response){
            $location.url('/sales');
        }

        function invoiceCreateError(error){
            Error(error)
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function doTraiter(){
            updateSale()
        }

        function doImprimer(){

        }

        /*    ---  Redirect ---    */
        function goGifts(){
            $location.url('/sales');
        }

    }

    function SaleCreateCtrl($mdToast, $state, $stateParams, $location, SaleService, InputService) {
        /* jshint validthis: true */
        var vm = this;
        var inputList = [];
        var total = 0;

        vm.create = createSale;
        vm.goProduct = goProduct;
        vm.prepareSale = prepareSale;
        vm.makeSale = makeSale;
        vm.inputList = inputList ;

        getInputs();

        /*    ---  Call API ---    */
        function getInputs(){
            InputService.getInputs()
                .then(getInputsSuccess, getInputsError);
        }

        function createSale (sale, reference) {
            SaleService.createSale(sale, reference)
                .then(createSaleSuccess, createSaleError);
        }

        /*    ---  Handle Response ---    */
        function getInputsSuccess(response){
            vm.inputs = response.data;
        }

        function getInputsError(error){
            Error(error)
        }

        function createSaleSuccess (response) {
            $state.transitionTo($state.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        }

        function createSaleError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function RandomNumber(){
            var number1 = new Date().getUTCMilliseconds() * new Date().getMilliseconds();
            var number2 = 9223372036854775807;
            return getRandomInt(number1, number2);
        }

        function getRandomInt(max, min) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function prepareSale(input){
            inputList.push(input);
            total = total + (input.quantitySale * input.price);
            vm.total = total;
        }

        function makeSale() {
            var reference = RandomNumber();

            for (var i = 0; i < inputList.length; i++) {
                createSale(inputList[i], reference);
            }
        }

        /*    ---  Redirect ---    */
        function goProduct(id){
            $location.url('/products/'+id);
        }
    }
})();
