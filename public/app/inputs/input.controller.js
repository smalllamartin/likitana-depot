(function () {
    'use strict';

    angular.module('likitana.input')
        .controller('InputReadsCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'InputService', InputReadsCtrl])
        .controller('InputCreateCtrl', ['$mdToast', '$location', '$stateParams', 'InputService', 'OriginService', InputCreateCtrl]);

    function InputReadsCtrl($mdToast, $mdDialog, $state, $stateParams, $location, InputService) {
        /* jshint validthis: true */
        var vm = this;
        vm.confirmSuppression = confirmSuppression;
        vm.goUpdate = goUpdate;
        vm.goProduct = goProduct;

        getInputs();

        /*    ---  Call API ---    */
        function getInputs(){
            InputService.getInputs()
                .then(getInputsSuccess, getInputsError);
        }

        function deleteInput(id) {
            InputService.deleteInput(id)
                .then(deleteInputSuccess, deleteInputError);
        }

        /*    ---  Handle Response ---    */
        function getInputsSuccess(response){
            vm.inputs = response.data;
        }

        function getInputsError(error){
            Error(error)
        }

        function deleteInputSuccess(response){
            Reload();
        }

        function deleteInputError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function confirmSuppression(ev, input){
            console.log(input);
            var confirm = $mdDialog.confirm()
                .title('Suppression')
                .content('Voulez vous supprimer cette catégorie ?')
                .ariaLabel('Suppression')
                .targetEvent(ev)
                .ok('Supprimer')
                .cancel('Annuler');
            $mdDialog.show(confirm).then(function () {
               deleteInput(input.id);
            });
        }

        function Reload(){
            $state.transitionTo($state.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        }

        /*    ---  Redirect ---    */
        function goUpdate(id){
            $location.url('/inputs/update/'+id);
        }

        function goProduct(id){
            $location.url('/products/'+id);
        }

    }

    function InputCreateCtrl($mdToast, $location, $stateParams, InputService, OriginService) {
        /* jshint validthis: true */
        var vm = this;
        vm.create = createInput;

        var id = $stateParams.id;

        getOrigins();

        /*    ---  Call API ---    */
        function createInput (input) {
            InputService.createInput(input, id)
                .then(createInputSuccess, createInputError);
        }

        function getOrigins(){
            OriginService.getOrigins()
                .then(getOriginsSuccess, getOriginsError);
        }

        /*    ---  Handle Response ---    */
        function getOriginsSuccess(response){
            vm.origins = response.data;
        }

        function getOriginsError(error){
            Error(error)
        }

        function createInputSuccess (response) {
            $location.url('/inputs');
        }

        function createInputError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }
    }

})();
