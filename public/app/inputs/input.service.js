(function () {
    'use strict';

    angular.module('likitana.input')
        .service("InputService", InputService);

    function InputService($http, $cookies){
        this.getInputs = getInputs;
        this.getInputsProduct = getInputsProduct;
        this.createInput = createInput;
        this.deleteInput = deleteInput;

        function getInputs(){
            return  $http.get('/inputs',
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function getInputsProduct(idProduct){
            return  $http.get('/inputs/products/'+idProduct,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function createInput(input, id){
            return $http.post('/inputs',
                {
                    "product.id": id,
                    "origin.id": input.origin_id,
                    "lot": input.lot,
                    "fabriquant": input.fabriquant,
                    "dateFabrication": input.dateFabrication,
                    "datePeremption": input.datePeremption,
                    "quantity": input.quantity,
                    "type": input.type,
                    "price": input.price
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function deleteInput(id){
            return  $http.delete('/inputs/' + id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }
    }

})();
