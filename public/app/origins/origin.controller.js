(function () {
    'use strict';

    angular.module('likitana.origin')
        .controller('OriginReadsCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'OriginService', OriginReadsCtrl])
        .controller('OriginCreateCtrl', ['$mdToast', '$location', 'OriginService', OriginCreateCtrl])
        .controller('OriginUpdateCtrl', ['$location', '$mdToast', '$stateParams', 'OriginService', OriginUpdateCtrl]);

    function OriginReadsCtrl($mdToast, $mdDialog, $state, $stateParams, $location, OriginService) {
        /* jshint validthis: true */
        var vm = this;
        vm.confirmSuppression = confirmSuppression;
        vm.goUpdate = goUpdate;

        getOrigins();

        /*    ---  Call API ---    */
        function getOrigins(){
            OriginService.getOrigins()
                .then(getOriginsSuccess, getOriginsError);
        }

        function deleteOrigin(id) {
            OriginService.deleteOrigin(id)
                .then(deleteOriginSuccess, deleteOriginError);
        }

        /*    ---  Handle Response ---    */
        function getOriginsSuccess(response){
            vm.origins = response.data;
        }

        function getOriginsError(error){
            Error(error)
        }

        function deleteOriginSuccess(response){
            Reload();
        }

        function deleteOriginError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function confirmSuppression(ev, origin){
            var confirm = $mdDialog.confirm()
                .title('Suppression')
                .content('Voulez vous supprimer cette catégorie ?')
                .ariaLabel('Suppression')
                .targetEvent(ev)
                .ok('Supprimer')
                .cancel('Annuler');
            $mdDialog.show(confirm).then(function () {
                deleteOrigin(origin.id);
            });
        }

        function Reload(){
            $state.transitionTo($state.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        }

        /*    ---  Redirect ---    */
        function goUpdate(id){
            $location.url('/origins/update/'+id);
        }

    }

    function OriginCreateCtrl($mdToast, $location, OriginService) {
        /* jshint validthis: true */
        var vm = this;
        vm.create = createOrigin;

        /*    ---  Call API ---    */
        function createOrigin (origin) {
            OriginService.createOrigin(origin)
                .then(createOriginSuccess, createOriginError);
        }

        /*    ---  Autres ---    */
        function createOriginSuccess (response) {
            $location.url('/origins');
        }

        function createOriginError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }
    }

    function OriginUpdateCtrl($location, $mdToast, $stateParams, OriginService) {
        /* jshint validthis: true */
        var vm = this;
        vm.update = updateOrigin;

        var id = $stateParams.id;

        getOrigin();

        /*    ---  Call API ---    */
        function getOrigin(){
            OriginService.getOrigin(id)
                .then(getOriginSuccess, getOriginError);
        }

        function updateOrigin(origin) {
            OriginService.updateOrigin(origin, id)
                .then(updateOriginSuccess, updateOriginError);
        }

        /*    ---  Handle Response ---    */
        function getOriginSuccess(response){
            vm.origin = response.data;
        }

        function getOriginError(error){
            Error(error);
        }

        function updateOriginSuccess (response){
            $location.url('/origins');
        }

        function updateOriginError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(3000)
            );
        }

    }
})();
