(function () {
    'use strict';

    angular.module('likitana.origin')
        .service("OriginService", OriginService);

    function OriginService($http, $cookies){
        this.getOrigins = getOrigins;
        this.getOrigin = getOrigin;
        this.createOrigin = createOrigin;
        this.updateOrigin = updateOrigin;
        this.deleteOrigin = deleteOrigin;

        function getOrigins(){
            return  $http.get('/origins',
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function getOrigin(id){
            return  $http.get('/origins/'+id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function createOrigin(origin){
            return $http.post('/origins',
                {
                    "origin": origin.origin
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function updateOrigin(origin, id){
            return  $http.put('/origins',
                {
                    "id": id,
                    "origin": origin.origin
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function deleteOrigin(id){
            return  $http.delete('/origins/' + id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }
    }

})();
