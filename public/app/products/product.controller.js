(function () {
    'use strict';

    angular.module('likitana.product')
        .controller('ProductReadsCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'ProductService', ProductReadsCtrl])
        .controller('ProductReadCtrl', ['$mdToast', '$stateParams', 'ProductService', 'InputService', ProductReadCtrl])
        .controller('ProductCreateCtrl', ['$mdToast', '$location', 'ProductService', 'CategorieService', 'ProgramService', ProductCreateCtrl])
        .controller('ProductUpdateCtrl', ['$location', '$mdToast', '$stateParams', 'ProductService', ProductUpdateCtrl]);

    function ProductReadsCtrl($mdToast, $mdDialog, $state, $stateParams, $location, ProductService) {
        /* jshint validthis: true */
        var vm = this;
        vm.confirmSuppression = confirmSuppression;
        vm.goUpdate = goUpdate;
        vm.goDetail = goDetail;
        vm.goCreateEntrer = goCreateEntrer;

        getProducts();

        /*    ---  Call API ---    */
        function getProducts(){
            ProductService.getProducts()
                .then(getProductsSuccess, getProductsError);
        }

        function deleteProduct(id) {
            ProductService.deleteProduct(id)
                .then(deleteProductSuccess, deleteProductError);
        }

        /*    ---  Handle Response ---    */
        function getProductsSuccess(response){
            vm.products = response.data;
        }

        function getProductsError(error){
            Error(error)
        }

        function deleteProductSuccess(response){
            Reload();
        }

        function deleteProductError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function confirmSuppression(ev, product){
            var confirm = $mdDialog.confirm()
                .title('Suppression')
                .content('Voulez vous supprimer cette catégorie ?')
                .ariaLabel('Suppression')
                .targetEvent(ev)
                .ok('Supprimer')
                .cancel('Annuler');
            $mdDialog.show(confirm).then(function () {
                deleteProduct(product.id);
            });
        }

        function Reload(){
            $state.transitionTo($state.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        }

        /*    ---  Redirect ---    */
        function goUpdate(id){
            $location.url('/products/update/'+id);
        }

        function goDetail(id){
            $location.url('/products/'+id);
        }

        function goCreateEntrer(id){
            $location.url('/products/'+id+'/inputs/new');
        }

    }

    function ProductReadCtrl($mdToast,$stateParams, ProductService, InputService) {
        /* jshint validthis: true */
        var vm = this;

        var id = $stateParams.id;

        getProduct();
        getInputsProduct();

        /*    ---  Call API ---    */
        function getProduct(){
            ProductService.getProduct(id)
                .then(getProductsSuccess, getProductsError);
        }

        function getInputsProduct(){
            InputService.getInputsProduct(id)
                .then(InputsProductSuccess, InputsProductError);
        }
        /*    ---  Handle Response ---    */
        function getProductsSuccess(response){
            vm.product = response.data;
        }

        function getProductsError(error){
            Error(error)
        }

        function InputsProductSuccess(response){
            vm.inputs = response.data;
        }

        function InputsProductError(error){
            Error(error)
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }
    }

    function ProductCreateCtrl($mdToast, $location, ProductService, CategorieService, ProgramService) {
        /* jshint validthis: true */
        var vm = this;
        vm.create = createProduct;

        getCategories();
        getPrograms();

        /*    ---  Call API ---    */
        function createProduct (product) {
            ProductService.createProduct(product)
                .then(createProductSuccess, createProductError);
        }

        function getCategories(){
            CategorieService.getCategories()
                .then(getCategoriesSuccess, getCategoriesError);
        }

        function getPrograms(){
            ProgramService.getPrograms()
                .then(getProgramsSuccess, getProgramsError);
        }

        /*    ---  Handle Response ---    */
        function getCategoriesSuccess(response){
            vm.categories = response.data;
        }

        function getCategoriesError(error){
            Error(error)
        }

        function getProgramsSuccess(response){
            vm.programs = response.data;
        }

        function getProgramsError(error){
            Error(error)
        }

        /*    ---  Autres ---    */
        function createProductSuccess (response) {
            $location.url('/products');
        }

        function createProductError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }
    }

    function ProductUpdateCtrl($location, $mdToast, $stateParams, ProductService) {
        /* jshint validthis: true */
        var vm = this;
        vm.update = updateProduct;

        var id = $stateParams.id;

        getProduct();

        /*    ---  Call API ---    */
        function getProduct(){
            ProductService.getProduct(id)
                .then(getProductSuccess, getProductError);
        }

        function updateProduct(product) {
            ProductService.updateProduct(product, id)
                .then(updateProductSuccess, updateProductError);
        }

        /*    ---  Handle Response ---    */
        function getProductSuccess(response){
            vm.product = response.data;
        }

        function getProductError(error){
            Error(error);
        }

        function updateProductSuccess (response){
            $location.url('/products');
        }

        function updateProductError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(3000)
            );
        }

    }
})();
