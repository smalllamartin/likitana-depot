(function () {
    'use strict';

    angular.module('likitana.product')
        .service("ProductService", ProductService);

    function ProductService($http, $cookies){
        this.getProducts = getProducts;
        this.getProduct = getProduct;
        this.createProduct = createProduct;
        this.updateProduct = updateProduct;
        this.deleteProduct = deleteProduct;

        function getProducts(){
            return  $http.get('/products',
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function getProduct(id){
            return  $http.get('/products/'+id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function createProduct(product){
            return $http.post('/products',
                {
                    "designation": product.designation,
                    "classePharmacologique": product.classePharmacologique,
                    "type": product.type,
                    "formeGalenique": product.formeGalenique,
                    "dosage": product.dosage,
                    "conditionnement": product.conditionnement,
                    "conservation": product.conservation,
                    "categorie.id": product.categorie_id,
                    "program.id": product.program_id
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function updateProduct(product, id){
            return  $http.put('/products',
                {
                    "id": id,
                    "designation": product.designation,
                    "classePharmacologique": product.classePharmacologique,
                    "type": product.type,
                    "formeGalenique": product.formeGalenique,
                    "dosage": product.dosage,
                    "conditionnement": product.conditionnement,
                    "conservation": product.conservation,
                    "categorie.id": product.categorie.id,
                    "program.id": product.program.id
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function deleteProduct(id){
            return  $http.delete('/products/' + id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }
    }

})();
