(function () {
    'use strict';

    angular.module('likitana.user')
        .service("UserService", UserService);

    function UserService($http, $cookies){
        this.getUsers = getUsers;
        this.getUser = getUser;
        this.createUser = createUser;
        this.updateUser = updateUser;
        this.deleteUser = deleteUser;

        function getUsers(){
            return  $http.get('/users',
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function getUser(id){
            return  $http.get('/users/'+id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function createUser(user){
            return $http.post('/users',
                {
                    "nom": user.nom,
                    "prenom": user.prenom,
                    "telephone": user.telephone,
                    "password": user.telephone,
                    "profil": user.profil
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function updateUser(user, id){
            return  $http.put('/users',
                {
                    "id": id,
                    "nom": user.nom,
                    "prenom": user.prenom,
                    "telephone": user.telephone,
                    "password": user.password,
                    "profil": user.profil
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function deleteUser(id){
            return  $http.delete('/users/' + id,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }
    }

})();
