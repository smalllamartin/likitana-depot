(function () {
    'use strict';

    angular.module('likitana.user')
        .controller('UserReadsCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'UserService', UserReadsCtrl])
        .controller('UserCreateCtrl', ['$mdToast', '$location', 'UserService', UserCreateCtrl])
        .controller('UserUpdateCtrl', ['$location', '$mdToast', '$stateParams', 'UserService', UserUpdateCtrl]);

    function UserReadsCtrl($mdToast, $mdDialog, $state, $stateParams, $location, UserService) {
        /* jshint validthis: true */
        var vm = this;
        vm.confirmSuppression = confirmSuppression;
        vm.goUpdate = goUpdate;

        getUsers();

        /*    ---  Call API ---    */
        function getUsers(){
            UserService.getUsers()
                .then(getUsersSuccess, getUsersError);
        }

        function deleteUser(id) {
            UserService.deleteUser(id)
                .then(deleteUserSuccess, deleteUserError);
        }

        /*    ---  Handle Response ---    */
        function getUsersSuccess(response){
            vm.users = response.data;
        }

        function getUsersError(error){
            Error(error)
        }

        function deleteUserSuccess(response){
            Reload();
        }

        function deleteUserError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function confirmSuppression(ev, user){
            var confirm = $mdDialog.confirm()
                .title('Suppression')
                .content('Voulez vous supprimer cette catégorie ?')
                .ariaLabel('Suppression')
                .targetEvent(ev)
                .ok('Supprimer')
                .cancel('Annuler');
            $mdDialog.show(confirm).then(function () {
                deleteUser(user.id);
            });
        }

        function Reload(){
            $state.transitionTo($state.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        }

        /*    ---  Redirect ---    */
        function goUpdate(id){
            $location.url('/users/update/'+id);
        }

    }

    function UserCreateCtrl($mdToast, $location, UserService) {
        /* jshint validthis: true */
        var vm = this;
        vm.create = createUser;

        /*    ---  Call API ---    */
        function createUser (user) {
            UserService.createUser(user)
                .then(createUserSuccess, createUserError);
        }

        /*    ---  Autres ---    */
        function createUserSuccess (response) {
            $location.url('/users');
        }

        function createUserError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }
    }

    function UserUpdateCtrl($location, $mdToast, $stateParams, UserService) {
        /* jshint validthis: true */
        var vm = this;
        vm.update = updateUser;

        var id = $stateParams.id;

        getUser();

        /*    ---  Call API ---    */
        function getUser(){
            UserService.getUser(id)
                .then(getUserSuccess, getUserError);
        }

        function updateUser(user) {
            UserService.updateUser(user, id)
                .then(updateUserSuccess, updateUserError);
        }

        /*    ---  Handle Response ---    */
        function getUserSuccess(response){
            vm.user = response.data;
        }

        function getUserError(error){
            Error(error);
        }

        function updateUserSuccess (response){
            $location.url('/users');
        }

        function updateUserError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(3000)
            );
        }

    }
})();
