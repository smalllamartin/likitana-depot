(function () {
    'use strict';

    angular.module('likitana')
        .factory('CrudFactory', function ($resource) {
            return $resource('/categories', {}, {
                query: {method: 'GET', isArray: true},
                create: {method: 'POST'}
            })
        })
        .factory('CrudFactory', function ($resource) {
            return $resource('/categories/:id', {}, {
                show: {method: 'GET'},
                update: {method: 'PUT', params: {id: '@id'}},
                delete: {method: 'DELETE', params: {id: '@id'}}
            })
        });
})(); 