(function () {
    'use strict';

    angular.module('likitana')
        .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
            var routes, setRoutes;

            routes = [
                'chart/charts',
                'chart/echarts', 'chart/echarts-line', 'chart/echarts-bar', 'chart/echarts-pie', 'chart/echarts-scatter', 'chart/echarts-more'
            ];

            setRoutes = function(route) {
                var config, url;
                url = '/' + route;
                config = {
                    url: url,
                    templateUrl: 'app/' + route + '.html'
                };
                $stateProvider.state(route, config);
                return $stateProvider;
            };

            routes.forEach(function(route) {
            return setRoutes(route);
            });

            $urlRouterProvider
                .when('', '/login')
                .when('/', '/login')
                .otherwise('/accueil');

            /*  ------------------------ login ------------------------ */
            $stateProvider.state('login', {
                url: '/login',
                templateUrl: 'app/login/login.html'
            });

            /*  ------------------------ accueil ------------------------ */
            $stateProvider.state('accueil', {
                url: '/accueil',
                templateUrl: 'app/layout/accueil.html'
            });

            /*  ------------------------ categories ------------------------ */
            $stateProvider.state('categories', {
                url: '/categories',
                templateUrl: 'app/categories/reads.html'
            });

            $stateProvider.state('categories-create', {
                url: '/categories/new',
                templateUrl: 'app/categories/create.html'
            });

            $stateProvider.state('categories-update', {
                url: '/categories/update/{id:int}',
                templateUrl: 'app/categories/update.html'
            });

            /*  ------------------------ destinations ------------------------ */
            $stateProvider.state('destinations', {
                url: '/destinations',
                templateUrl: 'app/destinations/reads.html'
            });

            $stateProvider.state('destinations-create', {
                url: '/destinations/new',
                templateUrl: 'app/destinations/create.html'
            });

            $stateProvider.state('destinations-update', {
                url: '/destinations/update/{id:int}',
                templateUrl: 'app/destinations/update.html'
            });

            /*  ------------------------ programs ------------------------ */
            $stateProvider.state('programs', {
                url: '/programs',
                templateUrl: 'app/programs/reads.html'
            });

            $stateProvider.state('programs-create', {
                url: '/programs/new',
                templateUrl: 'app/programs/create.html'
            });

            $stateProvider.state('programs-update', {
                url: '/programs/update/{id:int}',
                templateUrl: 'app/programs/update.html'
            });

            /*  ------------------------ origins ------------------------ */
            $stateProvider.state('origins', {
                url: '/origins',
                templateUrl: 'app/origins/reads.html'
            });

            $stateProvider.state('origins-create', {
                url: '/origins/new',
                templateUrl: 'app/origins/create.html'
            });

            $stateProvider.state('origins-update', {
                url: '/origins/update/{id:int}',
                templateUrl: 'app/origins/update.html'
            });

            /*  ------------------------ outputs ------------------------ */
            $stateProvider.state('outputs', {
                url: '/outputs',
                templateUrl: 'app/outputs/reads.html'
            });

            /*  ------------------------ users ------------------------ */
            $stateProvider.state('users', {
                url: '/users',
                templateUrl: 'app/users/reads.html'
            });

            $stateProvider.state('users-create', {
                url: '/users/new',
                templateUrl: 'app/users/create.html'
            });

            $stateProvider.state('users-update', {
                url: '/users/update/{id:int}',
                templateUrl: 'app/users/update.html'
            });

            /*  ------------------------ products ------------------------ */
            $stateProvider.state('products', {
                url: '/products',
                templateUrl: 'app/products/reads.html'
            });

            $stateProvider.state('product', {
                url: '/products/{id:int}',
                templateUrl: 'app/products/read.html'
            });

            $stateProvider.state('products-create', {
                url: '/products/new',
                templateUrl: 'app/products/create.html'
            });

            $stateProvider.state('products-update', {
                url: '/products/update/{id:int}',
                templateUrl: 'app/products/update.html'
            });

            /*  ------------------------ inputs ------------------------ */
            $stateProvider.state('inputs', {
                url: '/inputs',
                templateUrl: 'app/inputs/reads.html'
            });

            $stateProvider.state('inputs-create', {
                url: '/products/{id:int}/inputs/new',
                templateUrl: 'app/inputs/create.html'
            });

            /*  ------------------------ invoices ------------------------ */
            $stateProvider.state('invoices', {
                url: '/invoices',
                templateUrl: 'app/invoices/reads.html'
            });

            $stateProvider.state('invoice', {
                url: '/invoices/{reference:int}',
                templateUrl: 'app/invoices/read.html'
            });

            /*  ------------------------ gifts ------------------------ */
            $stateProvider.state('gifts', {
                url: '/gifts',
                templateUrl: 'app/gifts/reads.html'
            });

            $stateProvider.state('gift', {
                url: '/gifts/{reference:int}',
                templateUrl: 'app/gifts/read.html'
            });

            $stateProvider.state('gifts-create', {
                url: '/gifts/new',
                templateUrl: 'app/gifts/create.html'
            });

            /*  ------------------------ sales ------------------------ */
            $stateProvider.state('sales', {
                url: '/sales',
                templateUrl: 'app/sales/reads.html'
            });

            $stateProvider.state('sale', {
                url: '/sales/{reference:int}',
                templateUrl: 'app/sales/read.html'
            });

            $stateProvider.state('sales-create', {
                url: '/sales/new',
                templateUrl: 'app/sales/create.html'
            });

            /*  ------------------------ rapports ------------------------ */
            $stateProvider.state('rapports-produits', {
                url: '/rapports/produits',
                templateUrl: 'app/rapport/produits.html'
            });

            $stateProvider.state('rapports-lots', {
                url: '/rapports/lots',
                templateUrl: 'app/rapport/lots.html'
            });

            $stateProvider.state('ruptures', {
                url: '/rapports/ruptures',
                templateUrl: 'app/rapport/ruptures.html'
            });

            $stateProvider.state('peremptions', {
                url: '/rapports/peremptions',
                templateUrl: 'app/rapport/peremptions.html'
            });

            $stateProvider.state('rapports-origins', {
                url: '/rapports/origins',
                templateUrl: 'app/rapport/origins.html'
            });

            $stateProvider.state('rapports-destinations', {
                url: '/rapports/destinations',
                templateUrl: 'app/rapport/destinations.html'
            });

            $stateProvider.state('rapports-status', {
                url: '/rapports/status',
                templateUrl: 'app/rapport/status.html'
            });

            $stateProvider.state('stocks-mouvements', {
                url: '/rapports/stocks/{id:int}',
                templateUrl: 'app/rapport/stock-mouvement.html'
            });

            $stateProvider.state('lots', {
                url: '/rapports/lots',
                templateUrl: 'app/rapport/lot.html'
            });

            $stateProvider.state('lots-mouvements', {
                url: '/rapports/lots/{numeroLot:string}',
                templateUrl: 'app/rapport/lot-mouvement.html'
            });

        }]
    );

})(); 