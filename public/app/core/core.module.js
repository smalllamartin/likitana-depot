(function () {
    'use strict';

    angular.module('likitana.core', [
        // Angular modules
         'ngAnimate'
        ,'ngAria'
        ,'ngMessages'
        , 'ngCookies'
        , 'ngResource'
        , 'angular.filter'

        // Custom modules
        ,'app.layout'
        ,'app.i18n'
        
        // 3rd Party Modules
        ,'ngMaterial'
        ,'ui.router'
        ,'ui.bootstrap'
        ,'duScroll'
    ]);

})();

