(function () {

    angular.module('app.i18n', ['pascalprecht.translate'])
        .config(['$translateProvider', i18nConfig])
        .controller('LangCtrl', ['$scope', '$translate', LangCtrl]);

    function i18nConfig($translateProvider) {

        $translateProvider.useStaticFilesLoader({
            prefix: 'i18n/',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('fr');
        $translateProvider.useSanitizeValueStrategy(null);
    }

    function LangCtrl($scope, $translate) {
        $scope.lang = 'French';
        $scope.setLang = setLang;
        $scope.getFlag = getFlag;


        function setLang (lang) {
            switch (lang) {
                case 'French':
                    $translate.use('fr');
                    break;
                case 'English':
                    $translate.use('en');
                    break;
            }
            return $scope.lang = lang;
        }
        function getFlag() {
            var lang;
            lang = $scope.lang;
            switch (lang) {
                case 'French':
                    return 'flags-france';
                    break;
                case 'English':
                    return 'flags-american';
                    break;
            }
        }
    }

})(); 
