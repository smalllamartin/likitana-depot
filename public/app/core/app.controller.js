(function () {
    'use strict';

    angular.module('likitana')
        .controller('AppCtrl', ['$scope', '$rootScope', '$state', '$document', 'appConfig', '$cookies', '$location', '$http', '$mdToast', AppCtrl]); // overall control
    
    function AppCtrl($scope, $rootScope, $state, $document, appConfig, $cookies, $location,$http,  $mdToast) {

        $scope.pageTransitionOpts = appConfig.pageTransitionOpts;
        $scope.main = appConfig.main;
        $scope.color = appConfig.color;

        $scope.$watch('main', function(newVal, oldVal) {
            // if (newVal.menu !== oldVal.menu || newVal.layout !== oldVal.layout) {
            //     $rootScope.$broadcast('layout:changed');
            // }

            if (newVal.menu === 'horizontal' && oldVal.menu === 'vertical') {
            $rootScope.$broadcast('nav:reset');
            }
            if (newVal.fixedHeader === false && newVal.fixedSidebar === true) {
            if (oldVal.fixedHeader === false && oldVal.fixedSidebar === false) {
                $scope.main.fixedHeader = true;
                $scope.main.fixedSidebar = true;
            }
            if (oldVal.fixedHeader === true && oldVal.fixedSidebar === true) {
                $scope.main.fixedHeader = false;
                $scope.main.fixedSidebar = false;
            }
            }
            if (newVal.fixedSidebar === true) {
            $scope.main.fixedHeader = true;
            }
            if (newVal.fixedHeader === false) {
            $scope.main.fixedSidebar = false;
            }
        }, true);


        $rootScope.$on("$stateChangeSuccess", function (event, currentRoute, previousRoute) {
            $document.scrollTo(0, 0);
        });

        $rootScope.logout = function(){
            $http.get('/logout',
                {
                      headers: {'X-Auth-Token': $cookies.get('authToken')}
                })
                .success(function (data, status, headers, config, statusText) {
                    if (status == '200' || status == '301') {
                        $location.url('/login');
                        $mdToast.show(
                            $mdToast.simple()
                                .content('Déconnexion')
                                .position('top right')
                                .hideDelay(3000)
                        );
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .content('Echec de déconnexion. Veuillez vérifier la connexion au systèm!')
                                .position('top right')
                                .hideDelay(3000)
                        );
                    }
                })
                .error(function (data, status, headers, config, statusText) {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Echec de chargement de la liste. Veuillez vérifier la connexion au système')
                            .position('top right')
                            .hideDelay(3000)
                    );
                });
        }
    }

})(); 