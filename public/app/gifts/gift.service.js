(function () {
    'use strict';

    angular.module('likitana.gift')
        .service("GiftService", GiftService);

    function GiftService($http, $cookies){
        this.getGifts = getGifts;
        this.getGift = getGift;
        this.createGift = createGift;
        this.updateGift = updateGift;

        function getGifts(){
            return  $http.get('/outputs/references',
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function getGift(reference){
            return  $http.get('/outputs/references/'+reference,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }

        function createGift(gift, reference){
            return $http.post('/gifts',
                {
                    "reference" : reference,
                    "id": gift.id,
                    "quantity" : gift.quantityGift
                },
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                }
            );
        }

        function updateGift(reference){
            return  $http.put('/outputs/references/'+reference,
                {
                    headers: {'X-Auth-Token': $cookies.get('authToken')}
                });
        }
    }

})();
