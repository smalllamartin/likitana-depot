(function () {
    'use strict';

    angular.module('likitana.gift')
        .controller('GiftReadsCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'GiftService', GiftReadsCtrl])
        .controller('GiftReadCtrl', ['$mdToast','$mdDialog', '$state', '$stateParams', '$location', 'GiftService', GiftReadCtrl])
        .controller('GiftCreateCtrl', ['$mdToast', '$state', '$stateParams', '$location',  'GiftService','ProductService', GiftCreateCtrl]);

    function GiftReadsCtrl($mdToast, $mdDialog, $state, $stateParams, $location, GiftService) {
        /* jshint validthis: true */
        var vm = this;
        vm.goTraiter = goTraiter;
        vm.doImprimer = doImprimer;

        getGifts();

        /*    ---  Call API ---    */
        function getGifts(){
            GiftService.getGifts()
                .then(getGiftsSuccess, getGiftsError);
        }

        /*    ---  Handle Response ---    */
        function getGiftsSuccess(response){
            vm.gifts = response.data;
        }

        function getGiftsError(error){
            Error(error)
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function doImprimer(){

        }

        /*    ---  Redirect ---    */
        function goTraiter(reference){
            $location.url('/gifts/'+reference);
        }

    }

    function GiftReadCtrl($mdToast, $mdDialog, $state, $stateParams, $location, GiftService) {
        /* jshint validthis: true */
        var vm = this;
        vm.doTraiter = doTraiter;
        vm.doImprimer = doImprimer;

        var reference = $stateParams.reference;

        getGifts();

        /*    ---  Call API ---    */
        function getGifts(){
            GiftService.getGift(reference)
                .then(getGiftsSuccess, getGiftsError);
        }

        function updateGift(){
            GiftService.updateGift(reference)
                .then(updateGiftSuccess, updateGiftError);
        }

        /*    ---  Handle Response ---    */
        function getGiftsSuccess(response){
            vm.gifts = response.data;
        }

        function getGiftsError(error){
            Error(error)
        }

        function updateGiftSuccess(response){
           goGifts();
        }

        function updateGiftError(error){
            Error(error)
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function doTraiter(){
            updateGift()
        }

        function doImprimer(){

        }

        /*    ---  Redirect ---    */
        function goGifts(){
            $location.url('/gifts');
        }

    }

    function GiftCreateCtrl($mdToast, $state, $stateParams, $location, GiftService, ProductService) {
        /* jshint validthis: true */
        var vm = this;
        var productList = [];

        vm.create = createGift;
        vm.goProduct = goProduct;
        vm.prepareGift = prepareGift;
        vm.makeGift = makeGift;
        vm.productList = productList ;

        getProducts();

        /*    ---  Call API ---    */
        function getProducts(){
            ProductService.getProducts()
                .then(getProductsSuccess, getProductsError);
        }

        function createGift (gift, reference) {
            GiftService.createGift(gift, reference)
                .then(createGiftSuccess, createGiftError);
        }

        /*    ---  Handle Response ---    */
        function getProductsSuccess(response){
            vm.products = response.data;
        }

        function getProductsError(error){
            Error(error)
        }

        function createGiftSuccess (response) {
            $state.transitionTo($state.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        }

        function createGiftError(error){
            Error(error);
        }

        /*    ---  Autres ---    */
        function Error(error){
            $mdToast.show(
                $mdToast.simple()
                    .content('Code error :  '+error.status)
                    .position('top right')
                    .hideDelay(4000)
            );
        }

        function RandomNumber(){
            var number1 = new Date().getUTCMilliseconds() * new Date().getMilliseconds();
            var number2 = 9223372036854775807;
            return getRandomInt(number1, number2);
        }

        function getRandomInt(max, min) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function prepareGift(product){
            productList.push(product);
        }

        function makeGift() {
            var reference = RandomNumber();

            for (var i = 0; i < productList.length; i++) {
                createGift(productList[i], reference);
            }
        }

        /*    ---  Redirect ---    */
        function goProduct(id){
            $location.url('/products/'+id);
        }
    }
})();
