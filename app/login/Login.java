package login;

import play.data.validation.Constraints;

/**
 * Created by brabo on 1/27/16.
 */
public class Login {
    @Constraints.Required
    private String telephone;
    @Constraints.Required
    @Constraints.MinLength(6)
    @Constraints.MaxLength(256)
    private String password;

    public Login(String telephone, String password) {
        this.telephone = telephone;
        this.password = password;
    }

    public Login() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
