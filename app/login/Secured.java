package login;

import models.User;
import play.Logger;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by brabo on 1/27/16.
 */
public class Secured extends Security.Authenticator {
    public String getUsername(Http.Context ctx) {
        Logger.info("Enter login.Secured");
        String token = getTokenFromHeader(ctx);
        if (token != null) {
            Logger.info("token = " + token);
            User utilisateur = (new User()).findByAuthToken(token);
            if (utilisateur != null) {
                Logger.info("utilisateur existe");
                ctx.args.put("utilisateur", utilisateur);
                return utilisateur.getTelephone();
            } else {
                Logger.info("utilisateur n'existe pas ");
            }
        }
        Logger.info("token == null");
        return null;
    }

    @Override
    public Result onUnauthorized(Http.Context context) {
        return super.onUnauthorized(context);
    }

    private String getTokenFromHeader(Http.Context ctx) {
        Logger.info("verification si l'utilsateur est connecté");
        String[] authTokenHeaderValues = ctx.request().headers().get("X-AUTH-TOKEN");
        if ((authTokenHeaderValues != null) && (authTokenHeaderValues.length == 1) && (authTokenHeaderValues[0] != null)) {
            return authTokenHeaderValues[0];
        }
        return null;
    }
}
