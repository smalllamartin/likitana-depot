package login;

import play.data.validation.Constraints;

/**
 * Created by brabo on 1/30/16.
 */
public class Signin {
    private Long id;
    @Constraints.Required
    private String nom;
    @Constraints.Required
    private String prenom;
    @Constraints.Required
    private String telephone;
    @Constraints.Required
    @Constraints.MinLength(6)
    @Constraints.MaxLength(256)
    private String password;
    @Constraints.Required
    private String profil;

    public Signin(String nom, String prenom, String telephone, String password, String profil) {
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.password = password;
        this.profil = profil;
    }

    public Signin() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
