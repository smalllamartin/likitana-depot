package controllers;

import login.Secured;
import models.Origin;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by brabo on 1/18/16.
 */
public class Origins extends Controller {
    /**
     * Action : Liste de toutes les origin
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result reads() {
        return ok(Json.toJson(new Origin().findList()));
    }

    /**
     * Action : détail d'une origin
     *
     * @param id
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result read(Long id) {
        Origin origin = new Origin().findById(id);
        if (origin != null) {
            return ok(Json.toJson(origin));
        } else {
            return badRequest();
        }
    }

    /**
     * Action : création d'une origin
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result create() {
        Form<Origin> form = Form.form(Origin.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Origin origin = form.get();
            String result = origin.create(origin);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action : mise à jour d'une origin
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result update() {
        Form<Origin> form = Form.form(Origin.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Origin origin = form.get();
            String result = origin.update(origin);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action : delete d'une origin
     *
     * @param id
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result delete(long id) {
        String result = new Origin().delete(id);
        if (result != null) {
            return badRequest();
        } else {
            return ok("done");
        }
    }
}
