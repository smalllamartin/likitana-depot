package controllers;

import login.Secured;
import models.Input;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by brabo on 1/18/16.
 */
public class Inputs extends Controller {
    /**
     * Action : Liste de toutes les inputs
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result reads() {
        return ok(Json.toJson(new Input().findList()));
    }

    /**
     * Action : détail d'une input
     *
     * @param id
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result read(Long id) {
        Input input = new Input().findById(id);
        if (input != null) {
            return ok(Json.toJson(input));
        } else {
            return badRequest();
        }
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public Result readsByProduct(Long idProduct) {
        return ok(Json.toJson(new Input().findListByProduct(idProduct)));
    }


    /**
     * Action : création d'une input
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result create() {
        Form<Input> form = Form.form(Input.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Input input = form.get();
            String result = input.create(input);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action : suppression d'une input
     *
     * @param id
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result delete(long id) {
        String result = new Input().delete(id);
        if (result != null) {
            return badRequest();
        } else {
            return ok("done");
        }
    }
}
