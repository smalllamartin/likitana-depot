package controllers;

import login.Secured;
import models.Invoice;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by brabo on 2/16/16.
 */
public class Invoices extends Controller {

    @Transactional
    @Security.Authenticated(Secured.class)
    public Result reads() {
        return ok(Json.toJson(new Invoice().findList()));
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public Result readsLastByReference() {
        return ok(Json.toJson(new Invoice().findListLastByReference()));
    }


    @Transactional
    @Security.Authenticated(Secured.class)
    public Result readsByReference(String reference) {
        return ok(Json.toJson(new Invoice().findListByReference(reference)));
    }

    /**
     * Action : create an invoice
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result create() {
        Form<Invoice> form = Form.form(Invoice.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Invoice invoice = form.get();
            String result = invoice.create(invoice);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }
}
