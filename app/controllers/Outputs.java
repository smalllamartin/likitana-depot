package controllers;

import login.Secured;
import models.Order;
import models.Output;
import models.pojo.GiftHelper;
import models.pojo.SaleHelper;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by brabo on 2/19/16.
 */
public class Outputs extends Controller{

    @Transactional
    @Security.Authenticated(Secured.class)
    public Result reads() {
        return ok(Json.toJson(new Output().findList()));
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public Result readsLastByReference() {
        return ok(Json.toJson(new Order().findListLastByReference()));
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public Result readsByReference(String reference) {
        return ok(Json.toJson(new Order().findListByReference(reference)));
    }

    /**
     * Action :
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result createSale() {
        Form<SaleHelper> form = Form.form(SaleHelper.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            SaleHelper saleHelper = form.get();
            String result = saleHelper.createSale(saleHelper);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action :
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result createGigt() {
        Form<GiftHelper> form = Form.form(GiftHelper.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            GiftHelper giftHelper = form.get();
            String result = giftHelper.createGigt(giftHelper);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action validation
     * @param reference
     * @return
     */
    @Transactional
    //@Security.Authenticated(Secured.class)
    public Result validation(String reference){
        String result = new Order().validation(reference);
        if (result != null) {
            return badRequest();
        } else {
            return ok("done");
        }
    }

    @Transactional
    //@Security.Authenticated(Secured.class)
    public Result update(Long id) {
        String result = new Output().update(id);
        if (result != null) {
            return badRequest();
        } else {
            return ok("done");
        }
    }

}
