package controllers;

import login.Secured;
import models.Product;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by brabo on 1/18/16.
 */
public class Products extends Controller {
    /**
     * Action : Liste de tous les produits
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result reads() {
        return ok(Json.toJson(new Product().findList()));
    }

    /**
     * Action : détail d'un produit
     *
     * @param id
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result read(Long id) {
        Product produit = new Product().findById(id);
        if (produit != null) {
            return ok(Json.toJson(produit));
        } else {
            return badRequest();
        }
    }

    /**
     * Action : création d'un produit
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result create() {
        Form<Product> form = Form.form(Product.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Product produit = form.get();
            String result = produit.create(produit);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action : mise à jour d'un produit
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result update() {
        Form<Product> form = Form.form(Product.class).bindFromRequest();
        if (form.hasErrors()) {
            return ok("false");
        } else {
            Product produit = form.get();
            String result = produit.update(produit);
            if (result != null) {
                return ok("false");
            } else {
                return ok("true");
            }
        }
    }

    /**
     * Action : delete d'une produit
     *
     * @param id
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result delete(long id) {
        String result = new Product().delete(id);
        if (result != null) {
            return ok("false");
        } else {
            return ok("true");
        }
    }
}
