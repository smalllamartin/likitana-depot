package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import login.Login;
import login.Secured;
import models.User;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.WebSocket;
import views.html.index;

public class Application extends Controller {
    private static final String AUTH_TOKEN = "authToken";
    private static final String PROFIL = "profil";

    @Transactional
    public Result index() {
        User gerant = new User("tonton", "bachir", "123123", "123123", "gerant");
       // gerant.creation(gerant);
        return ok(index.render());
    }

    @Transactional
    public Result login() {
        Form<Login> formulaire = Form.form(Login.class).bindFromRequest();
        if (formulaire.hasErrors()) {
            return badRequest(formulaire.errorsAsJson());
        } else {

            Login login = formulaire.get();
            User utilisateur = (new User()).findByTelephoneAndPassword(login.getTelephone(), login.getPassword());

            if (utilisateur == null) {
                return unauthorized();
            } else {
                String authToken = utilisateur.createAuthToken(utilisateur);
                ObjectNode authTokenJson = Json.newObject();
                authTokenJson.put(AUTH_TOKEN, authToken);
                response().setCookie(AUTH_TOKEN, authToken);
                response().setCookie(PROFIL, utilisateur.getProfil());
                response().setHeader("X-AUTH-TOKEN", authToken);
                return ok(authTokenJson);
            }
        }
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public Result logout() {
        User utilisateur = (new User()).getCurrentUtilisateur();
        utilisateur.deleteAuthToken(utilisateur);
        response().discardCookie(AUTH_TOKEN);
        response().discardCookie(PROFIL);
        return ok("done");
    }
}
