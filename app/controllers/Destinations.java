package controllers;

import login.Secured;
import models.Destination;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by brabo on 1/18/16.
 */
public class Destinations extends Controller {
    /**
     * Action : Liste de toutes les destinations
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result reads() {
        return ok(Json.toJson(new Destination().findList()));
    }

    /**
     * Action : détail d'une destination
     *
     * @param id
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result read(Long id) {
        Destination destination = new Destination().findById(id);
        if (destination != null) {
            return ok(Json.toJson(destination));
        } else {
            return badRequest();
        }
    }

    /**
     * Action : création d'une destination
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result create() {
        Form<Destination> form = Form.form(Destination.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Destination destination = form.get();
            String result = destination.create(destination);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action : mise à jour d'une destination
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result update() {
        Form<Destination> form = Form.form(Destination.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Destination destination = form.get();
            String result = destination.update(destination);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action : delete d'une destination
     *
     * @param id
     * @return
     */
    @Transactional
    public Result delete(long id) {
        String result = new Destination().delete(id);
        if (result != null) {
            return badRequest();
        } else {
            return ok("done");
        }
    }
}
