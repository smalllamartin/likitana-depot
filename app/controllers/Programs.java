package controllers;

import login.Secured;
import models.Program;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by brabo on 1/28/16.
 */
public class Programs extends Controller {
    /**
     * Action : Liste de toutes les programmes
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result reads() {
        return ok(Json.toJson(new Program().findList()));
    }

    /**
     * Action : détail d'une programme
     *
     * @param id
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result read(Long id) {
        Program programme = new Program().findById(id);
        if (programme != null) {
            return ok(Json.toJson(programme));
        } else {
            return badRequest();
        }
    }

    /**
     * Action : création d'une programme
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result create() {
        Form<Program> form = Form.form(Program.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Program programme = form.get();
            String result = programme.create(programme);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action : mise à jour d'une programme
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result update() {
        Form<Program> form = Form.form(Program.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Program programme = form.get();
            String result = programme.update(programme);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action : delete d'une programme
     *
     * @param id
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result delete(long id) {
        String result = new Program().delete(id);
        if (result != null) {
            return badRequest();
        } else {
            return ok("done");
        }
    }
}
