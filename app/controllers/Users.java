package controllers;

import login.Signin;
import models.User;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by brabo on 1/30/16.
 */
public class Users extends Controller {
    /**
     * Action : Liste de toutes les utilisateurs
     *
     * @return
     */
    @Transactional
    public Result reads() {
        return ok(Json.toJson(new User().findList()));
    }

    /**
     * Action : détail d'une utilisateur
     *
     * @param id
     * @return
     */
    @Transactional
    public Result read(Long id) {
        User utilisateur = new User().findById(id);
        if (utilisateur != null) {
            return ok(Json.toJson(utilisateur));
        } else {
            return badRequest();
        }
    }

    /**
     * Action : création d'une utilisateur
     *
     * @return
     */
    @Transactional
    public Result create() {
        Form<Signin> form = Form.form(Signin.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Signin signin = form.get();
            User utilisateur = new User(signin.getNom(), signin.getPrenom(), signin.getTelephone(), signin.getPassword(), signin.getProfil());
            String result = utilisateur.creation(utilisateur);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action : mise à jour d'une utilisateur
     *
     * @return
     */
    @Transactional
    public Result update() {
        Form<Signin> form = Form.form(Signin.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Signin signin = form.get();
            User utilisateur = new User(signin.getNom(), signin.getPrenom(), signin.getTelephone(), signin.getPassword(), signin.getProfil());
            utilisateur.setId(signin.getId());
            System.out.println(utilisateur.getId());
            String result = utilisateur.modification(utilisateur);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action : suppression d'une utilisateur
     *
     * @param id
     * @return
     */
    @Transactional
    public Result delete(long id) {
        String result = new User().suppression(id);
        if (result != null) {
            return badRequest();
        } else {
            return ok("done");
        }
    }
}
