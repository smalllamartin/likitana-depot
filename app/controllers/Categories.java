package controllers;

import login.Secured;
import models.Categorie;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by brabo on 1/18/16.
 */
public class Categories extends Controller {

    /**
     * Action : Liste de toutes les catégorie
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result reads() {
        return ok(Json.toJson(new Categorie().findList()));
    }

    /**
     * Action : détail d'une catégorie
     *
     * @param id
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result read(Long id) {
        Categorie categorie = new Categorie().findById(id);
        if (categorie != null) {
            return ok(Json.toJson(categorie));
        } else {
            return badRequest();
        }
    }

    /**
     * Action : création d'une catégorie
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result create() {
        Form<Categorie> form = Form.form(Categorie.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Categorie categorie = form.get();
            String result = categorie.create(categorie);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action : mise à jour d'une catégorie
     *
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result update() {
        Form<Categorie> form = Form.form(Categorie.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Categorie categorie = form.get();
            String result = categorie.update(categorie);
            if (result != null) {
                return badRequest();
            } else {
                return ok("done");
            }
        }
    }

    /**
     * Action : delete d'une catégorie
     *
     * @param id
     * @return
     */
    @Transactional
    @Security.Authenticated(Secured.class)
    public Result delete(long id) {
        String result = new Categorie().delete(id);
        if (result != null) {
            return badRequest();
        } else {
            return ok("done");
        }
    }

}
