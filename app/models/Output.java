package models;

import models.pojo.GiftHelper;
import models.pojo.SaleHelper;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by brabo on 2/17/16.
 */
@Entity
@Table(name = "output")
public class Output extends Movement {
    @ManyToOne
    protected Destination destination;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "status")
    protected Long status;


    @Transient
    private Long stock;


    /**
     * Constructor with arguments
     * @param product
     * @param origin
     * @param lot
     * @param fabriquant
     * @param dateFabrication
     * @param datePeremption
     * @param type
     * @param quantity
     * @param price
     * @param destination
     */
    public Output(Product product, Origin origin, String lot, String fabriquant, Date dateFabrication, Date datePeremption, String type, Long quantity, Long price, String whoDone, Date whenDone, Destination destination, Long status) {
        this.product = product;
        this.origin = origin;
        this.lot = lot;
        this.fabriquant = fabriquant;
        this.dateFabrication = dateFabrication;
        this.datePeremption = datePeremption;
        this.type = type;
        this.quantity = quantity;
        this.price = price;
        this.whoDone = whoDone;
        this.whenDone = whenDone;
        this.destination = destination;
        this.status = status;
    }

    /**
     * Constructor withoud arguments
     */
    public Output() {
    }

    /**
     * Find outputs
     * @return
     */
    public List<Output> findList() {
        List<Output> outputList ;
        List<Output> outputListOutput = new ArrayList<>();
        try {
            outputList =  JPA.em().createQuery("select output From Output output").getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
        for(Output output : outputList){
            output.setStock( new StockLot().findByLot(output.getLot()).getStock());
            outputListOutput.add(output);
        }
        return outputListOutput;
    }

    /**
     * Find output by id
     * @param id
     * @return
     */
    public Output findById(Long id) {
        try {
            return (Output) JPA.em().createQuery("select output From Output output WHERE output.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }
    /**
     * Save output
     * @param output
     * @return
     */
    public String create(Output output) {
        String result = null;
        try {
            JPA.em().persist(output);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    /**
     * Validation des sorties réells des stocks
     * @param id
     * @return
     */
    public String update(Long id) {
        String result = null;
        Output output = findById(id);
        if(output != null){
            try {
                output.setStatus(1l);
                JPA.em().persist(output);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
        }else{
            result = "Aucun enregistrement";
        }
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

}
