package models;

import play.data.validation.Constraints;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.*;
import java.util.List;

/**
 * Created by brabo on 1/18/16.
 */
@Entity
@Table(name = "categorie")
public class Categorie {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Constraints.Required
    @Column(name = "categorie", unique=true)
    private String categorie;

    /**
     * Constructeur avec arguments
     *
     * @param categorie
     */
    public Categorie(String categorie) {
        this.categorie = categorie;
    }

    /**
     * Constructeur sans arugument
     */
    public Categorie() {
    }

    /**
     * Liste des catégories
     *
     * @return
     */
    public List<Categorie> findList() {
        try {
            return JPA.em().createQuery("select categorie From Categorie categorie").getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Retrouver une catégorie en fonction de son id
     *
     * @param id
     * @return
     */
    public Categorie findById(Long id) {
        try {
            return (Categorie) JPA.em().createQuery("select categorie From Categorie categorie WHERE categorie.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Enregistrement d'une catégorie
     *
     * @param categorie
     * @return
     */
    public String create(Categorie categorie) {
        String result = null;
        try {
            JPA.em().persist(categorie);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    /**
     * Mise à jour de la catégorie
     *
     * @param categorie
     * @return
     */
    public String update(Categorie categorie) {
        Categorie newCategorie = new Categorie().findById(categorie.getId());
        if (newCategorie == null) {
            return "aucun enregistrement correspondant";
        } else {
            String result = null;
            newCategorie.setCategorie(categorie.getCategorie());
            try {
                JPA.em().persist(newCategorie);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }
    }

    /**
     * Suppression de la catégorie
     *
     * @param id
     * @return
     */
    public String delete(Long id) {
        String result = null;
        Categorie categorie = new Categorie().findById(id);
        if (categorie == null) {
            return "aucun enregistrement correspondant";
        } else {
            try {
                JPA.em().remove(categorie);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }
    }

    /**
     * Validation de l'objet
     *
     * @return
     */
    public String validate() {
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }
}
