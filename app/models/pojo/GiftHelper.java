package models.pojo;

import models.Input;
import models.Order;
import java.util.Date;
import java.util.List;

/**
 * Created by brabo on 2/19/16.
 */
public class GiftHelper {
    private String reference;
    private Long id;
    private Long quantity;

    public String createGigt(GiftHelper giftHelper) {
        String result = null;

        Long temoin = giftHelper.getQuantity();
        Long quantitySortie;

        List<Input> inputList = new Input().findListByProduct(giftHelper.getId());
        for(Input input : inputList){
            Long quantityInput = input.getStock();
            if(temoin > 0){
                if(temoin - quantityInput <= 0 ){
                    quantitySortie = temoin;
                    temoin = 0l;
                    Order order = new Order(input.getLot(), giftHelper.getReference(), quantitySortie, 0l, 0l, 0l, "gift", "123123", new Date());
                    result = order.create(order);
                }else{
                    quantitySortie = quantityInput;
                    temoin = temoin - quantitySortie;
                    Order order = new Order(input.getLot(), giftHelper.getReference(), quantitySortie, 0l, 0l, 0l, "gift", "123123", new Date());
                    result = order.create(order);
                }
            }
        }
        return result;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
