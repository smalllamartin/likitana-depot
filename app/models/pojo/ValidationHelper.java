package models.pojo;

/**
 * Created by brabo on 2/22/16.
 */
public class ValidationHelper {
    private Long destination;
    private String type;

    public ValidationHelper(Long destination, String type) {
        this.destination = destination;
        this.type = type;
    }

    public ValidationHelper() {
    }

    public Long getDestination() {
        return destination;
    }

    public void setDestination(Long destination) {
        this.destination = destination;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
