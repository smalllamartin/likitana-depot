package models.pojo;

import models.Categorie;
import models.Program;

/**
 * Created by brabo on 2/18/16.
 */
public class ProductHelper {
    private Categorie categorie;
    private Program program;
    private Long id;
    private String designation;
    private String classePharmacologique;
    private String type;
    private String formeGalenique;
    private String dosage;
    private String conditionnement;
    private String conservation;
    private Long stock;

    public ProductHelper(Categorie categorie, Program program, Long id, String designation, String classePharmacologique, String type, String formeGalenique, String dosage, String conditionnement, String conservation, Long stock) {
        this.categorie = categorie;
        this.program = program;
        this.id = id;
        this.designation = designation;
        this.classePharmacologique = classePharmacologique;
        this.type = type;
        this.formeGalenique = formeGalenique;
        this.dosage = dosage;
        this.conditionnement = conditionnement;
        this.conservation = conservation;
        this.stock = stock;
    }

    public ProductHelper() {
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getClassePharmacologique() {
        return classePharmacologique;
    }

    public void setClassePharmacologique(String classePharmacologique) {
        this.classePharmacologique = classePharmacologique;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFormeGalenique() {
        return formeGalenique;
    }

    public void setFormeGalenique(String formeGalenique) {
        this.formeGalenique = formeGalenique;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getConditionnement() {
        return conditionnement;
    }

    public void setConditionnement(String conditionnement) {
        this.conditionnement = conditionnement;
    }

    public String getConservation() {
        return conservation;
    }

    public void setConservation(String conservation) {
        this.conservation = conservation;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }
}
