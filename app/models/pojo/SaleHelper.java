package models.pojo;

import models.Order;
import java.util.Date;

/**
 * Created by brabo on 2/19/16.
 */
public class SaleHelper {
    private String reference;
    private String lot;
    private Long quantity;
    private Long price;

    public String createSale(SaleHelper saleHelper) {
        Order order = new Order(saleHelper.getLot(), saleHelper.getReference(), saleHelper.getQuantity(), saleHelper.getPrice(), (saleHelper.getQuantity() * saleHelper.getPrice()), 0l, "sale", "123123", new Date());
        return order.create(order);
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
