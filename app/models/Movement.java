package models;

import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Created by brabo on 1/21/16.
 */
@MappedSuperclass
public class Movement extends Lot implements Serializable {
    @Column(name = "type")
    protected String type;    // (Achat, don)  (vente, don, perimé, cassé, perdu)
    @Column(name = "quantity")
    protected Long quantity;
    @Column(name = "price")
    protected Long price;     // prix de vente
    /**
     * who done
     */
    @Column(name = "who_done")
    protected String whoDone;
    /**
     * When Done
     */
    @Column(name = "when_done")
    protected Date whenDone;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getWhoDone() {
        return whoDone;
    }

    public void setWhoDone(String whoDone) {
        this.whoDone = whoDone;
    }

    public Date getWhenDone() {
        return whenDone;
    }

    public void setWhenDone(Date whenDone) {
        this.whenDone = whenDone;
    }
}
