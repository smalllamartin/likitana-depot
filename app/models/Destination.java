package models;

import play.data.validation.Constraints;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.*;
import java.util.List;

/**
 * Created by brabo on 1/18/16.
 */
@Entity
@Table(name = "destination")
public class Destination {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;
    @Constraints.Required
    @Column(name = "destination", unique=true)
    private String destination;

    /**
     * Constructeur avec arguments
     *
     * @param destination
     */
    public Destination(String destination) {
        this.destination = destination;
    }

    /**
     * Constructeur sans arugument
     */
    public Destination() {
    }

    /**
     * Liste des destinations
     *
     * @return
     */
    public List<Destination> findList() {
        try {
            return JPA.em().createQuery("select destination From Destination destination").getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Retrouver une destination en fonction de son id
     *
     * @param id
     * @return
     */
    public Destination findById(Long id) {
        try {
            return (Destination) JPA.em().createQuery("select destination From Destination destination WHERE destination.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Enregistrement d'une destination
     *
     * @param destination
     * @return
     */
    public String create(Destination destination) {
        String result = null;
        try {
            JPA.em().persist(destination);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    /**
     * Mise à jour de la destination
     *
     * @param destination
     * @return
     */
    public String update(Destination destination) {
        Destination newDestination = new Destination().findById(destination.getId());
        if (newDestination == null) {
            return "aucun enregistrement correspondant";
        } else {
            String result = null;
            newDestination.setDestination(destination.getDestination());
            try {
                JPA.em().persist(newDestination);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }
    }

    /**
     * Suppression de la destination
     *
     * @param id
     * @return
     */
    public String delete(Long id) {
        String result = null;
        Destination destination = new Destination().findById(id);
        if (destination == null) {
            return "aucun enregistrement correspondant";
        } else {
            try {
                JPA.em().remove(destination);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }
    }

    /**
     * Validation de l'objet
     *
     * @return
     */
    public String validate() {
        return null;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
