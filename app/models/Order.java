package models;

import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by brabo on 1/18/16.
 */
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "lot")
    private String lot;
    @Column(name = "reference")
    private String reference;
    @Column(name = "quantity")
    private Long quantity;
    @Column(name = "price")     // prix payé
    private Long price;
    @Column(name = "total")     // prix total de la facture
    private Long total;
    @Column(name = "status")
    private Long status;
    @Column(name = "type")      // gift, sale
    private String type;
    /**
     * who done
     */
    @Column(name = "who_done")
    private String whoDone;
    /**
     * When Done
     */
    @Column(name = "when_done")
    private Date whenDone;

    /**
     * Constructor with arguments
     * @param lot
     * @param reference
     * @param quantity
     * @param price
     * @param total
     * @param status
     * @param whoDone
     * @param whenDone
     */
    public Order(String lot, String reference, Long quantity, Long price, Long total, Long status, String type, String whoDone, Date whenDone) {
        this.lot = lot;
        this.reference = reference;
        this.quantity = quantity;
        this.price = price;
        this.total = total;
        this.status = status;
        this.type = type;
        this.whoDone = whoDone;
        this.whenDone = whenDone;
    }

    /**
     * Constructor without arguments
     */
    public Order() {
    }

    /**
     * Find orders
     * @return
     */
    private List<Order> findList() {
        try {
            return JPA.em().createQuery("select orders From Order orders").getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }
    /**
     * Find order by id
     * @param id
     * @return
     */
    private Order findById(Long id) {
        try {
            return (Order) JPA.em().createQuery("select orders From Order orders WHERE orders.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Find orders by reference
     *
     * @param reference
     * @return
     */
    public List<Order> findListByReference(String reference) {
        try {
            return JPA.em().createQuery("select orders From Order orders WHERE orders.reference = :reference").setParameter("reference", reference).getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Find last order by reference
     *
     * @param reference
     * @return
     */
    private Order findLastByReference(String reference) {
        List<Order> orderList = findListByReference(reference);
        Long temoin = (long) 0;
        for(Order order : orderList){
            Long id = order.getId();
            if (id > temoin) {
                temoin = id;
            }
        }
        if (temoin != 0) {
            return findById(temoin);
        } else {
            return null;
        }
    }

    /**
     * Find last orders
     * @return
     */
    public List<Order> findListLastByReference(){
        List<Order> orderListOut = new ArrayList<>();
        HashSet<Order> orderHashSet = new HashSet<>();
        List<Order> orderList = findList();

        assert orderList != null;
        orderHashSet.addAll(orderList.stream().map(order -> findLastByReference(order.getReference())).collect(Collectors.toList()));

        orderListOut.addAll(orderHashSet.stream().collect(Collectors.toList()));

        return orderListOut;
    }

    /**
     * create order
     *
     * @param order
     * @return
     */
    public String create(Order order) {
        System.out.println(order.getWhenDone());
        String result = null;
        try {
            JPA.em().persist(order);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    /**
     *
     * @param reference
     * @return
     */
    public String validation(String reference) {
        String result;
        String resultStockLot;
        String resultStockProduct;
        String resultOutput;
        Long stockTotal = 0l;
        List<Order> orderList = findListByReference(reference);
        for(Order order : orderList){
            // update Order
            order.setStatus(1l);
            result = order.update(order);
            if(result != null){
                return result;
            }

            // update stockLot
            StockLot stockLot = new StockLot().findByLot(order.getLot());
            stockLot.setStock(stockLot.getStock() - order.getQuantity());
            resultStockLot = stockLot.update(stockLot);
            if(resultStockLot != null){
                return resultStockLot;
            }

            // prepare update stockProduct
            stockTotal += order.getQuantity();

            // create output
            Input input = new Input().findByLot(order.getLot());
            Output output = new Output(input.getProduct(), input.getOrigin(), input.getLot(), input.getFabriquant(), input.getDateFabrication(), input.getDatePeremption(), "", order.getQuantity(), input.getPrice(), "123123", new Date(), null, 0l);
            resultOutput =  output.create(output);
            if(resultOutput != null){
                return resultOutput;
            }

        }

        // update stockProduct
        if(orderList.size() > 0){
            Input input = new Input().findByLot(orderList.get(0).getLot());
            StockProduct stockProduct = new StockProduct().findByProduct(input.getProduct().getId());
            stockProduct.setStock(stockProduct.getStock() - stockTotal);
            resultStockProduct =  stockProduct.update(stockProduct);
            if(resultStockProduct != null){
                return resultStockProduct;
            }
        }

        return null;
    }

    /**
     * Update Order
     * @param order
     * @return
     */
    private String update (Order order){
        String result = null;
        try {
            JPA.em().persist(order);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWhoDone() {
        return whoDone;
    }

    public void setWhoDone(String whoDone) {
        this.whoDone = whoDone;
    }

    public Date getWhenDone() {
        return whenDone;
    }

    public void setWhenDone(Date whenDone) {
        this.whenDone = whenDone;
    }
}
