package models;

import play.data.validation.Constraints;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.*;
import java.util.List;

/**
 * Created by brabo on 1/28/16.
 */
@Entity
@Table(name = "program")
public class Program {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Constraints.Required
    @Column(name = "program", unique=true)
    private String program;

    /**
     * Constructeur avec arguments
     *
     * @param program
     */
    public Program(String program) {
        this.program = program;
    }

    /**
     * Constructeur sans arugument
     */
    public Program() {
    }

    /**
     * Liste des programs
     *
     * @return
     */
    public List<Program> findList() {
        try {
            return JPA.em().createQuery("select program From Program program").getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Retrouver une program en fonction de son id
     *
     * @param id
     * @return
     */
    public Program findById(Long id) {
        try {
            return (Program) JPA.em().createQuery("select program From Program program WHERE program.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Enregistrement d'une program
     *
     * @param program
     * @return
     */
    public String create(Program program) {
        String result = null;
        try {
            JPA.em().persist(program);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    /**
     * Mise à jour de la program
     *
     * @param program
     * @return
     */
    public String update(Program program) {
        Program newProgram = new Program().findById(program.getId());
        if (newProgram == null) {
            return "aucun enregistrement correspondant";
        } else {
            String result = null;
            newProgram.setProgram(program.getProgram());
            try {
                JPA.em().persist(newProgram);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }
    }

    /**
     * Suppression de la program
     *
     * @param id
     * @return
     */
    public String delete(Long id) {
        String result = null;
        Program program = new Program().findById(id);
        if (program == null) {
            return "aucun enregistrement correspondant";
        } else {
            try {
                JPA.em().remove(program);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }
    }

    /**
     * Validation de l'objet
     *
     * @return
     */
    public String validate() {
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }
}
