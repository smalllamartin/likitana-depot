package models;

import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by brabo on 2/16/16.
 */
@Entity
@Table(name = "invoice")
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "reference")
    private String reference;
    @Column(name = "type")
    private String type;        // partiel, total
    @Column(name = "nature")
    private String nature;      // espèce, chèque, virement
    @Column(name = "montant")     // prix total du lot
    private Long montant;
    /**
     * who done
     */
    @Column(name = "who_done")
    private String whoDone;
    /**
     * When Done
     */
    @Column(name = "when_done")
    private Date whenDone;

    /**
     * Find Invoices
     * @return
     */
    public List<Invoice> findList() {
        try {
            return JPA.em().createQuery("select invoice From Invoice invoice").getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Find Invoice by id
     * @param id
     * @return
     */
    public Invoice findById(Long id) {
        try {
            return (Invoice) JPA.em().createQuery("select invoice From Invoice invoice WHERE invoice.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Find Invoices by reference
     * @param reference
     * @return
     */
    public List<Invoice> findListByReference(String reference) {
        try {
            return JPA.em().createQuery("select invoice From Invoice invoice WHERE invoice.reference = :reference").setParameter("reference", reference).getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Find last Invoice by reference
     * @param reference
     * @return
     */
    public Invoice findLastByReference(String reference) {
        List<Invoice> invoiceList = findListByReference(reference);
        Long temoin = (long) 0;
        for(Invoice invoice : invoiceList){
            Long id = invoice.getId();
            if (id > temoin) {
                temoin = id;
            }
        }
        if (temoin != 0) {
            return findById(temoin);
        } else {
            return null;
        }
    }

    /**
     * Find last invoices
     * @return
     */
    public List<Invoice> findListLastByReference(){
        List<Invoice> invoiceListOut = new ArrayList<>();
        HashSet<Invoice> invoiceHashSet = new HashSet<>();
        List<Invoice> invoiceList = findList();

        assert invoiceList != null;
        invoiceHashSet.addAll(invoiceList.stream().map(invoice -> findLastByReference(invoice.getReference())).collect(Collectors.toList()));

        invoiceListOut.addAll(invoiceHashSet.stream().collect(Collectors.toList()));

        return invoiceListOut;
    }

    /**
     * persist Invoice
     *
     * @param invoice
     * @return
     */
    public String create(Invoice invoice) {
        String result = null;
        try {
            invoice.setWhenDone(new Date());
            invoice.setWhoDone("123123");
            JPA.em().persist(invoice);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Long getMontant() {
        return montant;
    }

    public void setMontant(Long montant) {
        this.montant = montant;
    }

    public String getWhoDone() {
        return whoDone;
    }

    public void setWhoDone(String whoDone) {
        this.whoDone = whoDone;
    }

    public Date getWhenDone() {
        return whenDone;
    }

    public void setWhenDone(Date whenDone) {
        this.whenDone = whenDone;
    }
}
