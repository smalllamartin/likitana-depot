package models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by brabo on 2/17/16.
 */
@MappedSuperclass
public class Lot implements Serializable {
    @ManyToOne
    protected Product product;
    @ManyToOne
    protected Origin origin;
    @Column(name = "lot")
    protected String lot;
    @Column(name = "fabriquant")
    protected String fabriquant;
    @Column(name = "date_fabrication")
    protected Date dateFabrication;
    @Column(name = "date_peremption")
    protected Date datePeremption;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getFabriquant() {
        return fabriquant;
    }

    public void setFabriquant(String fabriquant) {
        this.fabriquant = fabriquant;
    }

    public Date getDateFabrication() {
        return dateFabrication;
    }

    public void setDateFabrication(Date dateFabrication) {
        this.dateFabrication = dateFabrication;
    }

    public Date getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(Date datePeremption) {
        this.datePeremption = datePeremption;
    }
}
