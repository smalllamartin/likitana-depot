package models;

import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.*;
import java.util.List;

/**
 * Created by brabo on 2/17/16.
 */
@Entity
@Table(name = "stock_lot")
public class StockLot {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "lot")
    private String lot;
    @Column(name = "stock")
    private Long stock;

    /**
     * Constructor with arguments
     * @param lot
     * @param stock
     */
    public StockLot(String lot, Long stock) {
        this.lot = lot;
        this.stock = stock;
    }

    /**
     * Constructor without arguments
     */
    public StockLot() {
    }

    /**
     * Liste des stockLots
     *
     * @return
     */
    public List<StockLot> findList() {
        try {
            return JPA.em().createQuery("select stockLot From StockLot stockLot").getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Retrouver une stockLot en fonction de son id
     *
     * @param id
     * @return
     */
    public StockLot findById(Long id) {
        try {
            return (StockLot) JPA.em().createQuery("select stockLot From StockLot stockLot WHERE stockLot.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Retrieve stock by lot
     *
     * @param lot
     * @return
     */
    public StockLot findByLot(String lot) {
        try {
            return (StockLot)JPA.em().createQuery("select stockLot From StockLot stockLot WHERE stockLot.lot = :lot").setParameter("lot", lot).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Enregistrement d'une stockLot
     *
     * @param stockLot
     * @return
     */
    public String create(StockLot stockLot) {
        String result = null;
        try {
            JPA.em().persist(stockLot);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    /**
     * Update stockLot
     * @param stockLot
     * @return
     */
    public String update(StockLot stockLot) {
        String result = null;
        try {
            JPA.em().persist(stockLot);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

}
