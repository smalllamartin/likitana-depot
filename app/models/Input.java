package models;

import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by brabo on 2/17/16.
 */
@Entity
@Table(name = "input")
public class Input extends Movement {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Transient
    private Long stock;

    /**
     * Finds Inputs
     *
     * @return
     */
    public List<Input> findList() {
        List<Input> inputList ;
        List<Input> inputListOutput = new ArrayList<>();
        try {
            inputList =  JPA.em().createQuery("select input From Input input").getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
        for(Input input : inputList){
            input.setStock( new StockLot().findByLot(input.getLot()).getStock());
            inputListOutput.add(input);
        }
        return inputListOutput;
    }

    /**
     * Retrieve Input by lot
     * @param lot
     * @return
     */
    public Input findByLot(String lot) {
        try {
            return (Input)JPA.em().createQuery("select input From Input input WHERE input.lot = :lot").setParameter("lot", lot).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }
    /**
     * Find Inputs by product id where stock is not null and order by date peremption
     * @return
     */
    public List<Input> findListByProduct(Long idProduct) {
        List<Input> inputList ;
        List<Input> inputListOutput = new ArrayList<>();
        try {
            inputList =  JPA.em().createQuery("select input From Input input WHERE  input.product.id = :idProduct order by input.datePeremption").setParameter("idProduct", idProduct).getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
        for(Input input : inputList){
            input.setStock( new StockLot().findByLot(input.getLot()).getStock());
            if(input.getStock() > 0){
                inputListOutput.add(input);
            }
        }
        return inputListOutput;
    }

    /**
     * Retrouver une input en fonction de son id
     *
     * @param id
     * @return
     */
    public Input findById(Long id) {
        try {
            return (Input) JPA.em().createQuery("select input From Input input WHERE input.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Enregistrement d'une input
     *
     * @param input
     * @return
     */
    public String create(Input input) {
        input.setWhenDone(new Date());
        String result = null;
        try {
            JPA.em().persist(input);

            StockLot stockLot = new StockLot(input.getLot(), input.getQuantity());
            stockLot.create(stockLot);

            StockProduct stockProduct = new StockProduct().findByProduct(input.getProduct().getId());
            stockProduct.setStock(stockProduct.getStock() + input.getQuantity());
            stockProduct.update(stockProduct);

        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    /**
     * Suppression de la input
     *
     * @param id
     * @return
     */
    public String delete(Long id) {
        String result = null;
        Input input = new Input().findById(id);
        if (input == null) {
            return "aucun enregistrement correspondant";
        } else {
            try {
                JPA.em().remove(input);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }
}
