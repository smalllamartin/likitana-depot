package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.data.validation.Constraints;
import play.db.jpa.JPA;
import play.mvc.Http;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by brabo on 1/27/16.
 */
@Entity
@Table(name = "users")
public class User {

    @Column(name = "creation_date", nullable = false)
    public Date creationDate;
    @Constraints.Required
    private String nom;
    @Constraints.Required
    private String prenom;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "auth_token")
    private String authToken;

    @Constraints.MaxLength(256)
    @Constraints.Required
    @Column(name = "telephone", length = 8, unique = true, nullable = false)
    private String telephone;

    @Column(name = "sha_password", length = 64, nullable = false)
    private byte[] shaPassword;

    @Transient
    @Constraints.MinLength(6)
    @Constraints.MaxLength(256)
    @JsonIgnore
    private String password;

    @Column(name = "profil")
    private String profil;

    public User() {
    }


    public User(String nom, String prenom, String telephone, String password, String profil) {
        this.nom = nom;
        this.prenom = prenom;
        setTelephone(telephone);
        setPassword(password);
        this.profil = profil;
        this.creationDate = new Date();
    }

    public static byte[] getSha512(String password) {
        try {
            return MessageDigest.getInstance("SHA-512").digest(password.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Liste des utilisateurs
     *
     * @return
     */
    @Transactional
    public List<User> findList() {
        try {
            return JPA.em().createQuery("select utilisateur From User utilisateur").getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Enregistrement d'un utilisateur
     *
     * @param utilisateur
     * @return
     */
    @Transactional
    public String creation(User utilisateur) {
        String result = null;
        try {
            JPA.em().persist(utilisateur);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    /**
     * Retrouver une utilisateur en fonction de son id
     *
     * @param id
     * @return
     */
    @Transactional
    public User findById(Long id) {
        try {
            return (User) JPA.em().createQuery("select utilisateur From User utilisateur WHERE utilisateur.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Suppression de la utilisateur
     *
     * @param id
     * @return
     */
    @Transactional
    public String suppression(Long id) {
        String result = null;
        User utilisateur = new User().findById(id);
        if (utilisateur == null) {
            return "aucun enregistrement correspondant";
        } else {
            try {
                JPA.em().remove(utilisateur);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }
    }

    /**
     * Mise à jour de la utilisateur
     *
     * @param utilisateur
     * @return
     */
    @Transactional
    public String modification(User utilisateur) {

        User newUtilisateur = new User().findById(utilisateur.getId());
        if (newUtilisateur == null) {
            return "aucun enregistrement correspondant";
        } else {
            String result = null;
            newUtilisateur.setNom(utilisateur.getNom());
            newUtilisateur.setPrenom(utilisateur.getPrenom());
            newUtilisateur.setPassword(utilisateur.getPassword());
            try {
                JPA.em().persist(newUtilisateur);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }
    }

    @Transactional
    public String createAuthToken(User utilisateur) {
        authToken = UUID.randomUUID().toString();
        utilisateur.setAuthToken(authToken);
        try {
            JPA.em().persist(utilisateur);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return authToken;
    }

    @Transactional
    public void deleteAuthToken(User utilisateur) {
        authToken = null;
        utilisateur.setAuthToken(authToken);
        try {
            JPA.em().persist(utilisateur);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    @Transactional
    public User findByAuthToken(String authToken) {
        if (authToken == null) {
            return null;
        }
        try {
            return (User) JPA.em().createQuery("select utilisateur From User utilisateur WHERE utilisateur.authToken = :authToken").setParameter("authToken", authToken).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    public User findByTelephoneAndPassword(String telephone, String password) {
        return (User) JPA.em().createQuery("select utilisateur From User utilisateur WHERE utilisateur.telephone = :telephone AND utilisateur.shaPassword = :shaPassword ").setParameter("telephone", telephone).setParameter("shaPassword", getSha512(password)).getSingleResult();
    }


    public User getCurrentUtilisateur() {
        return (User) Http.Context.current().args.get("utilisateur");
    }


    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone.toLowerCase();
    }

    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
        shaPassword = getSha512(password);
    }

    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }

    public String getAuthToken() {
        return authToken;
    }


    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
}