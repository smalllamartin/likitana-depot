package models;

import models.pojo.ProductHelper;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by brabo on 1/18/16.
 */
@Entity
@Table(name = "product")
public class Product {
    @ManyToOne
    private Categorie categorie;
    @ManyToOne
    private Program program;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "designation")
    private String designation;
    @Column(name = "classe_pharmacologique")
    private String classePharmacologique;
    @Column(name = "type")
    private String type;        //Catégorie de product:  médicament, réatifs, matériel médical, consommables, condoms, lubrifiants,
    @Column(name = "forme_galenique")
    private String formeGalenique;      // comprimé, solution buvable, solution injectable
    @Column(name = "dosage")
    private String dosage;
    @Column(name = "conditionnement")
    private String conditionnement;     // unité, boite, flacon
    @Column(name = "conservation")
    private String conservation;

    /**
     * Liste des products
     *
     * @return
     */
    public List<ProductHelper> findList() {
        List<Product>  productList;
        List<ProductHelper>  productHelperList = new ArrayList<>();
        try {
            productList = JPA.em().createQuery("select product From Product product").getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
        productHelperList.addAll(productList.stream().map(product -> new ProductHelper(product.getCategorie(), product.getProgram(), product.getId(), product.getDesignation(), product.getClassePharmacologique(), product.getType(), product.getFormeGalenique(), product.getDosage(), product.getConditionnement(), product.getConservation(), new StockProduct().findByProduct(product.getId()).getStock())).collect(Collectors.toList()));
        return productHelperList;
    }

    /**
     * Retrouver une product en fonction de son id
     *
     * @param id
     * @return
     */
    public Product findById(Long id) {
        try {
            return (Product) JPA.em().createQuery("select product From Product product WHERE product.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Enregistrement d'une product
     *
     * @param product
     * @return
     */
    public String create(Product product) {

        String result = null;
        try {
            JPA.em().persist(product);
            JPA.em().flush();
            StockProduct stockProduct = new StockProduct(product.getId(), 0l);
            stockProduct.create(stockProduct);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    /**
     * Mise à jour de la product
     *
     * @param product
     * @return
     */
    public String update(Product product) {
        Product newProduct = new Product().findById(product.getId());
        if (newProduct == null) {
            return "aucun enregistrement correspondant";
        } else {
            String result = null;
            newProduct.setCategorie(product.getCategorie());
            newProduct.setProgram(product.getProgram());
            newProduct.setClassePharmacologique(product.getClassePharmacologique());
            newProduct.setType(product.getType());
            newProduct.setFormeGalenique(product.getFormeGalenique());
            newProduct.setDosage(product.getDosage());
            newProduct.setConditionnement(product.getConditionnement());
            newProduct.setConservation(product.getConservation());
            newProduct.setDesignation(product.getDesignation());
            try {
                JPA.em().persist(newProduct);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }
    }

    /**
     * Suppression de la product
     *
     * @param id
     * @return
     */
    public String delete(Long id) {
        String result = null;
        Product product = new Product().findById(id);
        if (product == null) {
            return "aucun enregistrement correspondant";
        } else {
            try {
                JPA.em().remove(product);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }

    }

    /**
     * Validation de l'objet
     *
     * @return
     */
    public String validate() {
        return null;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getClassePharmacologique() {
        return classePharmacologique;
    }

    public void setClassePharmacologique(String classePharmacologique) {
        this.classePharmacologique = classePharmacologique;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFormeGalenique() {
        return formeGalenique;
    }

    public void setFormeGalenique(String formeGalenique) {
        this.formeGalenique = formeGalenique;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getConditionnement() {
        return conditionnement;
    }

    public void setConditionnement(String conditionnement) {
        this.conditionnement = conditionnement;
    }

    public String getConservation() {
        return conservation;
    }

    public void setConservation(String conservation) {
        this.conservation = conservation;
    }
}
