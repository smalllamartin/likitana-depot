package models;

import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.*;
import java.util.List;

/**
 * Created by brabo on 2/17/16.
 */
@Entity
@Table(name = "stock_product")
public class StockProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "product")
    private Long product;       // id product
    @Column(name = "stock")
    private Long stock;

    /**
     * Constructor with arguments
     * @param product
     * @param stock
     */
    public StockProduct(Long product, Long stock) {
        this.product = product;
        this.stock = stock;
    }

    /**
     * Constructor without arguments
     */
    public StockProduct() {
    }

    /**
     * Liste des stockProducts
     *
     * @return
     */
    public List<StockProduct> findList() {
        try {
            return JPA.em().createQuery("select stockProduct From StockProduct stockProduct").getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Retrouver une stockProduct en fonction de son id
     *
     * @param id
     * @return
     */
    public StockProduct findById(Long id) {
        try {
            return (StockProduct) JPA.em().createQuery("select stockProduct From StockProduct stockProduct WHERE stockProduct.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Retrieve stock by product
     *
     * @param product
     * @return
     */
    public StockProduct findByProduct(Long product) {
        try {
            return (StockProduct)JPA.em().createQuery("select stockProduct From StockProduct stockProduct WHERE stockProduct.product = :product").setParameter("product", product).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Enregistrement d'une stockProduct
     *
     * @param stockProduct
     * @return
     */
    public String create(StockProduct stockProduct) {
        String result = null;
        try {
            JPA.em().persist(stockProduct);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    /**
     * Mise à jour de la stockProduct
     *
     * @param stockProduct
     * @return
     */
    public String update(StockProduct stockProduct) {
        String result = null;
        try {
            JPA.em().persist(stockProduct);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProduct() {
        return product;
    }

    public void setProduct(Long product) {
        this.product = product;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }
}

