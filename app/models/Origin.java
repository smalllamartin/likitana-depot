package models;

import play.data.validation.Constraints;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;

import javax.persistence.*;
import java.util.List;

/**
 * Created by brabo on 1/18/16.
 */
@Entity
@Table(name = "origin")
public class Origin {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Constraints.Required
    @Column(name = "origin", unique=true)
    private String origin;

    /**
     * Constructeur avec arguments
     *
     * @param origin
     */
    public Origin(String origin) {
        this.origin = origin;
    }

    /**
     * Constructeur sans arugument
     */
    public Origin() {
    }

    /**
     * Liste des origins
     *
     * @return
     */
    public List<Origin> findList() {
        try {
            return JPA.em().createQuery("select origin From Origin origin").getResultList();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }

    }

    /**
     * Retrouver une origin en fonction de son id
     *
     * @param id
     * @return
     */
    public Origin findById(Long id) {
        try {
            return (Origin) JPA.em().createQuery("select origin From Origin origin WHERE origin.id = :id").setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Enregistrement d'une origin
     *
     * @param origin
     * @return
     */
    public String create(Origin origin) {
        String result = null;
        try {
            JPA.em().persist(origin);
        } catch (Exception e) {
            System.out.println(e.toString());
            result = e.toString();
        }
        return result;
    }

    /**
     * Mise à jour de la origin
     *
     * @param origin
     * @return
     */
    public String update(Origin origin) {
        Origin newOrigin = new Origin().findById(origin.getId());
        if (newOrigin == null) {
            return "aucun enregistrement correspondant";
        } else {
            String result = null;
            newOrigin.setOrigin(origin.getOrigin());
            try {
                JPA.em().persist(newOrigin);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }
    }

    /**
     * Suppression de la origin
     *
     * @param id
     * @return
     */
    public String delete(Long id) {
        String result = null;
        Origin origin = new Origin().findById(id);
        if (origin == null) {
            return "aucun enregistrement correspondant";
        } else {
            try {
                JPA.em().remove(origin);
            } catch (Exception e) {
                System.out.println(e.toString());
                result = e.toString();
            }
            return result;
        }
    }

    /**
     * Validation de l'objet
     *
     * @return
     */
    public String validate() {
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
}
